<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>
        <!-- Font Awesome -->
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
        />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
        />
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
        />
    </head>
    <body class="antialiased">
        <section class="h-100 h-custom" style="background-color: #8fc4b7;">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-lg-8 col-xl-6">
                        <div class="card rounded-3">
                            <img src="assets/image/img3.webp"
                                 class="w-100" style="border-top-left-radius: .3rem; border-top-right-radius: .3rem;"
                                 alt="Sample photo">
                            <div class="card-body p-4 p-md-5">
                                <div class="d-flex justify-content-between">
                                    <h3 class="mb-4 pb-2 pb-md-0 mb-md-5 px-md-2">Lấy số thứ tự</h3>
                                    <a class="btn btn-info" href="{{route('web.filter')}}" style="height: fit-content">Tra cứu STT</a>
                                </div>

                                <div class="px-md-2">
                                    <div class="mb-4">
                                        <input type="text" name="name"  class="form-control" placeholder="Tên" />
                                    </div>
                                    <div class="mb-4">
                                        <label class="text-danger">* Vui lòng điền số điện thoại của bạn</label>
                                        <input type="text" name="phone" maxlength="10" class="form-control validate" placeholder="Số Điện Thoại" required />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mb-4">
                                            <label class="text-danger"></label>
                                            <select class="form-select" name="sector">
                                                <option value="">Chọn lĩnh vực</option>
                                                @foreach($sector as $value)
                                                    <option value="{{$value->id}}">{{$value->linhvuc_ten}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <label class="text-danger">* Vui lòng chọn đơn vị</label>
                                            <select class="form-select" name="agency" required>
                                                <option value="">Chọn Đơn Vị</option>
                                                @foreach($agency as $_donvi)
                                                    <option value="{{$_donvi->id}}">{{$_donvi->donvi_ten}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <label class="text-danger">* Vui lòng chọn đơn vị xong chọn tiếp dịch vụ</label>
                                        <select class="form-select" name="service" required>
                                            <option value="">Chọn Dịch Vụ</option>
                                        </select>

                                    </div>
                                    <button type="submit" class="btn btn-success btn-lg mb-1">Lấy số thứ tự </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Lấy số thứ tự thành công</h5>
                    </div>
                    <div class="modal-body">
                        <div class="content page-break" id="html-content-holder" style="margin-bottom: 0px; padding:2px"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Vui lòng trả lời câu hỏi</h5>
                    </div>
                    <div class="modal-body">
                        <div class="d-flex align-items-center justify-content-center">
                            <p class="m-0" style="font-weight: bold;font-size: 20px;color: red"><span id="number_1" style="font-size: 20px"></span> + <span id="number_2" style="font-size: 20px"></span> = ? </p>
                            <input name="result" type="text" id="result" class="form-control" style="max-width: 100px;font-size: 20px">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                        <button type="button" class="btn btn-primary btn-get-stt">Xác nhận</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js" integrity="sha512-pumBsjNRGGqkPzKHndZMaAG+bir374sORyzM3uulLV14lN5LyykqNk8eEeUlUkB3U0M4FApyaHraT65ihJhDpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="assets/html2canvas.min.js" type="text/javascript"></script>
    <script src="assets/js/get-data.js"></script>
    <script src="assets/js/validate_number.js"></script>
</html>
