@if ($paginator->hasPages())
    <!-- Pagination -->
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @if ($paginator->onFirstPage())
                <li class="page-item disabled">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous">
                        <span aria-hidden="false">«</span>
                    </a>
                </li>
            @else
                @php
                    $previousPageUrl = $paginator->previousPageUrl();
                    $previousPageUrl = str_replace('0%5B','', $previousPageUrl);
                    $previousPageUrl = str_replace('%5D','', $previousPageUrl);
                @endphp
                <li class="page-item">
                    <a class="page-link" href="{{ $previousPageUrl }}" aria-label="Previous">
                        <span aria-hidden="true">«</span>
                    </a>
                </li>
            @endif
                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
                            @else
                                @php
                                    $url = str_replace('0%5B','', $url);
                                    $url = str_replace('%5D','', $url);
                                @endphp
                                <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @if ($paginator->hasMorePages())
                    @php
                        $next_url = $paginator->nextPageUrl();
                        $next_url = str_replace('0%5B','', $next_url);
                        $next_url = str_replace('%5D','', $next_url);
                    @endphp
                <li class="page-item">
                    <a class="page-link" href="{{ $next_url }}" aria-label="Next">
                        <span aria-hidden="true">»</span>
                    </a>
                </li>
            @else
                    <li class="page-item">
                        <button class="page-link" aria-label="Next">
                            <span aria-hidden="false">»</span>
                        </button>
                    </li>
            @endif
        </ul>
    </nav>
    <!-- Pagination -->
@endif
