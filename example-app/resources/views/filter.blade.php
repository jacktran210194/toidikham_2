<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>
    <!-- Font Awesome -->
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        rel="stylesheet"
    />
    <!-- MDB -->
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
        rel="stylesheet"
    />
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        rel="stylesheet"
    />
</head>
<body class="antialiased">
<section class="h-100 h-custom" style="background-color: #8fc4b7;min-height: 100vh">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-8 col-xl-6">
                <div class="card rounded-3">
                    <form method="get" action="{{route('web.filter')}}" style="position: relative">
                        <img src="assets/image/img3.webp"
                             class="w-100" style="border-top-left-radius: .3rem; border-top-right-radius: .3rem;"
                             alt="Sample photo">
                        <div style="position: absolute; top: 25px; left: 0;z-index: 10; width: 100%">
                            <div class="row">
                                <div class="col-6">
                                    <select class="form-select" name="agency">
                                        <option value="">Đơn Vị</option>
                                        @foreach($agency as $value)
                                            <option value="{{$value->id}}" @if(request()->get('agency') == $value->id) selected @endif>{{$value->donvi_ten}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <select class="form-select" name="service">
                                        <option value="">Dịch Vụ</option>
                                        @if(isset($service))
                                            @foreach($service as $item)
                                                <option value="{{$item->id}}" @if(request()->get('service') == $item->id) selected @endif>{{$item->dichvu_ten}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-4" style="justify-content: center">
                                <div class="col-6">
                                    <input class="form-control validate" name="stt_code" required type="text" value="{{request()->get('stt_code')}}" placeholder="Điền mã hoặc số điện thoại của bạn" >
                                </div>
                            </div>
                            <div class="row mt-4" style="justify-content: center">
                                <a href="/" class="btn btn-warning text-white" style="width: fit-content;margin-right: 15px">Trang chủ</a>
                                <button class="btn btn-primary" type="submit" style="width: fit-content">Tìm kiếm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            @if($customer)
                @foreach($customer as $_val)
                    <div class="col-lg-4 col-12  p-2">
                        <div style="background: #fff">
                            <div class="w-100">
                                <div class="p-3">
                                    <div style="border: 2px solid #000000;border-radius: 8px; padding: 5px">
                                        <h1 class="text-center m-0" style="font-size: 16px;font-weight: bold;color: #000000">{{$_val->ten_donvi}}</h1>
                                        @if(isset($_val->agency))
                                        <p class="m-0 text-center" style="font-size: 14px">{{$_val->agency->donvi_diachi}}</p>
                                        @endif
                                    </div>
                                    <div class="text-center mt-2">
                                        <p class="text-uppercase mb-0" style="font-size: 16px;font-weight: bold;color: #000000">Số thứ tự <br>{{$_val->ten_dichvu}}</p>
                                        <p class="m-0" style="font-size: 40px;font-weight: bold;color: #000000">{{$_val->khach_sothutu}}</p>
                                        <p class="m-0">@if(isset($_val->service->dichvu_ghichu)) {{$_val->service->dichvu_ghichu}} @endif</p>
                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <p class="m-0">Đang gọi: {{isset($_val->service) ? $_val->service->dichvu_sodanggoi : 0}}</p>
                                        <p class="m-0">Ngày : {{date_format(date_create($_val->khach_giolayso), 'd/m/Y')}} - Giờ lấy STT {{date_format(date_create($_val->khach_giolayso), 'H:i')}}</p>
                                        <p class="m-0">@if(isset($_val_khach_tenkhach)) {{$_val->khach_tenkhach}} - @endif {{$_val->khach_dienthoai}} - Mã STT : {{$_val->ma_sothutu}}</p>
                                    </div>
                                    <div style="display: flex;justify-content: center">
                                        {!! QrCode::size(90)->encoding('UTF-8')->generate($_val->text_qr) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js" integrity="sha512-pumBsjNRGGqkPzKHndZMaAG+bir374sORyzM3uulLV14lN5LyykqNk8eEeUlUkB3U0M4FApyaHraT65ihJhDpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="assets/js/get-data.js"></script>
<script src="assets/js/validate_number.js"></script>
</body>
</html>
