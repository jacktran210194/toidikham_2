<div style="background: #fff;border: 1px solid #333333">
    <div class="w-100">
        <div class="p-3">
            <div style="border: 2px solid #000000;border-radius: 8px; padding: 5px">
                <h1 class="text-center m-0" style="font-size: 16px;font-weight: bold;color: #000000">{{$customer->ten_donvi}}</h1>
                <p class="m-0 text-center" style="font-size: 14px">{{$agency->donvi_diachi}}</p>
            </div>
            <div class="text-center mt-2">
                <p class="text-uppercase mb-0" style="font-size: 16px;font-weight: bold;color: #000000">Số thứ tự <br>{{$customer->ten_dichvu}}</p>
                <p class="m-0" style="font-size: 40px;font-weight: bold;color: #000000">{{$customer->khach_sothutu}}</p>
                <p class="m-0">@if(isset($service)) {{$service->dichvu_ghichu}} @endif</p>
            </div>
            <hr>
            <div class="text-center">
                <p class="m-0">Đang gọi: {{$service->dichvu_sodanggoi}}</p>
                <p class="m-0">Ngày : {{date_format(date_create($customer->created_at), 'd/m/Y')}} - Giờ lấy STT {{date_format(date_create($customer->created_at), 'H:i')}}</p>
                <p class="m-0">@if(isset($customer->khach_tenkhach)) {{$customer->khach_tenkhach}} - @endif {{$customer->khach_dienthoai}} - Mã STT : {{$customer->khach_madatso}}</p>
            </div>
            <div style="display: flex;justify-content: center">
                {!! QrCode::size(90)->encoding('UTF-8')->generate($text_qrcode) !!}
            </div>
        </div>
    </div>
</div>
