<!DOCTYPE html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>{{$titlePage}}</title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
    <link href="theme/css/styles.css" rel="stylesheet" />
    <link rel="icon" type="image/x-icon" href="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/lotus.webp">
    <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
    @yield('page-style')
</head>
<body class="sb-nav-fixed">
@include('admin.layout.topnav')
<div id="layoutSidenav">
    @include('admin.layout.layoutSidenav')
    @yield('content')
</div>
@include('admin.layout.script')
@yield('script')
</body>
<!--end::Body-->
</html>
