<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                @foreach(config('menu.admin') as $menu)
                    @if(isset($menu['submenu']))
                        <a class="nav-link @if($menu['name'] != $namePage) collapsed @endif" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts__{{$menu['name']}}" aria-expanded="false" aria-controls="collapseLayouts">
                            <div class="sb-nav-link-icon"><i class="fas {{$menu['icon']}}"></i></div>
                            {{$menu['title']}}
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse @if($menu['name'] == $namePage) show @endif" id="collapseLayouts__{{$menu['name']}}" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                @foreach($menu['submenu'] as $submenu)
                                    <a class="nav-link @if(isset($subName) && $submenu['name'] == $subName) active @endif" href="{{route($submenu['route'])}}">{{$submenu['title']}}</a>
                                @endforeach
                            </nav>
                        </div>
                        @else
                        <a class="nav-link @if($menu['name'] == $namePage) active @endif" href="{{route($menu['route'])}}">
                            <div class="sb-nav-link-icon"><i class="fas {{$menu['icon']}}"></i></div>
                            {{$menu['title']}}
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </nav>
</div>
