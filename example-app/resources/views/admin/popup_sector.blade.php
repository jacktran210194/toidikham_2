<form action="{{route('admin.sector.update',$sector->id)}}" method="post">
    @csrf
    <div class="mb-3">
        <label class="mb-2">Tên lĩnh vực</label>
        <input name="linhvuc_ten" maxlength="255" value="{{$sector->linhvuc_ten}}" type="text" required class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Ghi chú</label>
        <textarea name="linhvuc_ghichu" maxlength="255" class="form-control">{{$sector->linhvuc_ghichu}}</textarea>
    </div>
    <div class="d-flex justify-content-center">
        <button class="btn btn-danger btn-close-modal" type="button" style="margin-right: 15px">Hủy</button>
        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Cập nhật</button>
    </div>
</form>
