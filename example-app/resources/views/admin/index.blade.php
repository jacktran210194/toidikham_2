@extends('admin.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container">
                <div class="p-3">
                    <h1>Danh sách đơn vị</h1>
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form action="{{route('admin.index')}}" method="get" class="d-flex align-items-center">
                        <input name="name" type="text" class="form-control" value="{{request()->get('name')}}" style="max-width: 300px; margin-right: 15px" placeholder="Điền tên đơn vị">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Tìm kiếm</button>
                        <button class="btn btn-info text-white" type="button" data-toggle="modal" data-target="#exampleModalCenter">+Tạo mới đơn vị</button>
                    </form>

                    <div class="row text-center" style="border: 1px solid #333333">
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Tên đơn vị</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Lĩnh vực</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Địa chỉ</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Ghi chú</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Bật/Tắt</p>
                        </div>
                        <div class="col-2">
                            <p class="m-0 p-1">***</p>
                        </div>
                    </div>
                    @if($listData->total() > 0)
                        @foreach($listData as $value)
                            <div class="row text-center" style="border: 1px solid #333333">
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->donvi_ten}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">@if(isset($value->sector)) {{$value->sector->linhvuc_ten}} @endif</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->donvi_diachi}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->donvi_ghichu}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">@if($value->donvi_isActive == 1) Bật @else Tắt @endif</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center p-2">
                                    <button class="btn btn-primary btn-edit" value="{{$value->id}}" style="margin-right: 10px">Sửa</button>
                                    <a href="{{route('admin.agency.delete',$value->id)}}" class="btn btn-danger btn-delete">Xóa</a>
                                </div>
                            </div>
                        @endforeach
                            <div class="d-flex justify-content-center mt-4">
                                {{ $listData->appends([request()->all()])->links('pagination_custom.index') }}
                            </div>
                        @else
                        <p class="text-danger text-center mt-3">Không có dữ liệu</p>
                    @endif
                </div>
            </div>
            <div class="modal fade" id="modalEditService" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Cập nhật đơn vị</h5>
                        </div>
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tạo mới đơn vị</h5>
                            <button type="button" class="close border-0 bg-transparent" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.agency.store')}}" method="post">
                                @csrf
                                <div class="mb-3">
                                    <label class="mb-2">Số điện thoại của đơn vị</label>
                                    <input name="phone" maxlength="10" type="text" required class="form-control validate">
                                </div>
                                <div class="mb-3">
                                    <label class="mb-2">Mật khẩu đăng nhập của đơn vị</label>
                                    <input name="password"  type="password" required class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="mb-2">Tên đơn vị</label>
                                    <input name="donvi_ten" maxlength="255" type="text" required class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="mb-2">Lĩnh vực</label>
                                    <select name="id_linhvuc" required class="form-select">
                                        <option value="">Chọn lĩnh vực</option>
                                        @foreach($sector as $value)
                                            <option value="{{$value->id}}">{{$value->linhvuc_ten}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="mb-2">Địa chỉ đơn vị</label>
                                    <input name="donvi_diachi" maxlength="255" type="text" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="mb-2">Ghi chú</label>
                                    <textarea name="donvi_ghichu" maxlength="255" class="form-control"></textarea>
                                </div>
                                <div class="mb-3">
                                    <div class="d-flex align-items-center">
                                        <label class="m-2" for="flexSwitchCheckChecked">Bật / Tắt</label>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" name="active" checked>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close" style="margin-right: 15px">Hủy</button>
                                    <button type="submit" class="btn btn-primary" style="margin-right: 15px">Tạo mới</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </main>
        @include('admin.layout.footer')
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-delete").click(function (ev) {
                ev.preventDefault();
                let url = $(this).attr('href');
                $.confirm({
                    title: 'Xác nhận!',
                    content: 'Bạn chắc xóa dịch vụ này không?',
                    buttons: {
                        confirm: {
                            text: 'Xác nhận',
                            btnClass: 'btn-blue',
                            action: function(){
                                location.replace(url);
                            }
                        },
                        cancel: {
                            text: 'Hủy',
                            btnClass: 'btn-danger',
                            action: function(){}
                        }
                    }
                });
            });
            $(".btn-edit").click(function () {
                let service_id = $(this).val();
                $.ajax({
                    url: window.location.origin + '/admin/show-agency',
                    data: {'id' : service_id},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.status){
                            $("#modalEditService .modal-body").html(data.html);
                            $("#modalEditService").modal("show");
                        }else{
                            alert(data.msg);
                        }
                    }
                })
            });
            $(document).on("click", ".btn-close-modal", function () {
                $("#modalEditService").modal("hide");
            });
        });
    </script>
@endsection
