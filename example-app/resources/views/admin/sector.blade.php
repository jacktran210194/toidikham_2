@extends('admin.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container">
                <div class="p-3">
                    <h1>Danh sách lĩnh vực</h1>
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form action="{{route('admin.sector.index')}}" method="get" class="d-flex align-items-center">
                        <input name="name" type="text" class="form-control" value="{{request()->get('name')}}" style="max-width: 400px; margin-right: 15px" placeholder="Điền tên lĩnh vực">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Tìm kiếm</button>
                        <button class="btn btn-info text-white" type="button" data-toggle="modal" data-target="#exampleModalCenter">+Tạo mới lĩnh vực</button>
                    </form>

                    <div class="row text-center" style="border: 1px solid #333333">
                        <div class="col-3" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Tên lĩnh vực</p>
                        </div>
                        <div class="col-3" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Ghi chú</p>
                        </div>
                        <div class="col-3" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Ngày tạo</p>
                        </div>
                        <div class="col-3">
                            <p class="m-0 p-1">***</p>
                        </div>
                    </div>
                    @if(count($listData))
                        @foreach($listData as $value)
                            <div class="row text-center" style="border: 1px solid #333333">
                                <div class="col-3 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->linhvuc_ten}}</p>
                                </div>
                                <div class="col-3 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->linhvuc_ghichu}}</p>
                                </div>
                                <div class="col-3 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{date_format(date_create($value->linhvuc_giotao), 'H:i d/m/Y')}}</p>
                                </div>
                                <div class="col-3 d-flex align-items-center justify-content-center p-2">
                                    <button class="btn btn-primary btn-edit" value="{{$value->id}}" style="margin-right: 10px">Sửa</button>
                                    <a href="{{route('admin.sector.delete',$value->id)}}" class="btn btn-danger btn-delete">Xóa</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p class="text-danger text-center mt-3">Không có dữ liệu</p>
                    @endif
                </div>
            </div>
            <div class="modal fade" id="modalEditService" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Cập nhật lĩnh vực</h5>
                        </div>
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tạo mới lĩnh vực</h5>
                            <button type="button" class="close border-0 bg-transparent" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.sector.store')}}" method="post">
                                @csrf
                                <div class="mb-3">
                                    <label class="mb-2">Tên lĩnh vực</label>
                                    <input name="linhvuc_ten" maxlength="255" type="text" required class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="mb-2">Ghi chú</label>
                                    <textarea name="linhvuc_ghichu" maxlength="255" class="form-control"></textarea>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close" style="margin-right: 15px">Hủy</button>
                                    <button type="submit" class="btn btn-primary" style="margin-right: 15px">Tạo mới</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @include('admin.layout.footer')
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-delete").click(function (ev) {
                ev.preventDefault();
                let url = $(this).attr('href');
                $.confirm({
                    title: 'Xác nhận!',
                    content: 'Bạn chắc xóa dịch vụ này không?',
                    buttons: {
                        confirm: {
                            text: 'Xác nhận',
                            btnClass: 'btn-blue',
                            action: function(){
                                location.replace(url);
                            }
                        },
                        cancel: {
                            text: 'Hủy',
                            btnClass: 'btn-danger',
                            action: function(){}
                        }
                    }
                });
            });
            $(".btn-edit").click(function () {
                let service_id = $(this).val();
                $.ajax({
                    url: window.location.origin + '/admin/sector/show',
                    data: {'id' : service_id},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.status){
                            $("#modalEditService .modal-body").html(data.html);
                            $("#modalEditService").modal("show");
                        }else{
                            alert(data.msg);
                        }
                    }
                })
            });
            $(document).on("click", ".btn-close-modal", function () {
                $("#modalEditService").modal("hide");
            });
        });
    </script>
@endsection
