<form action="{{route('admin.agency.update',$agency->id)}}" method="post">
    @csrf
    <h3 class="text-center">{{$agency->donvi_ten}}</h3>
    <div class="mb-3">
        <label class="mb-2">Tên đơn vị</label>
        <input name="donvi_ten" maxlength="255" value="{{$agency->donvi_ten}}" type="text" required class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Lĩnh vực</label>
        <select name="id_linhvuc" class="form-select">
            @foreach($sector as $value)
                <option value="{{$value->id}}" @if($value->id == $agency->id_linhvuc) selected @endif>{{$value->linhvuc_ten}}</option>
            @endforeach
        </select>
    </div>
    <div class="mb-3">
        <label class="mb-2">Địa chỉ đơn vị</label>
        <input name="donvi_diachi" maxlength="255" value="{{$agency->donvi_diachi}}" type="text" required class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Ghi chú</label>
        <textarea name="donvi_ghichu" maxlength="255" class="form-control">{{$agency->donvi_ghichu}}</textarea>
    </div>
    <div class="mb-3">
        <label class="mb-2">Bật / Tắt</label>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" name="active" @if($agency->donvi_isActive) checked @endif>
            <label class="form-check-label" for="flexSwitchCheckChecked">@if($agency->donvi_isActive) Đơn vị đang hoạt động @else Đơn vị tạm tắt @endif</label>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <button class="btn btn-danger btn-close-modal" type="button" style="margin-right: 15px">Hủy</button>
        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Cập nhật</button>
    </div>
</form>
