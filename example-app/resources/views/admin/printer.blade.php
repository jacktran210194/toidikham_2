@extends('admin.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container">
                <div class="p-3">
                    <h1>Máy in</h1>
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form action="{{route('admin.printer.index')}}" method="get" class="d-flex align-items-center">
                        <select name="id_donvi" class="form-select" style="max-width: 200px;margin-right: 15px">
                            <option value="">Chọn đơn vị </option>
                            @foreach($listAgency as $agency)
                                <option value="{{$agency->id}}" @if($agency->id == request()->get('id_donvi')) selected @endif>{{$agency->donvi_ten}}</option>
                            @endforeach
                        </select>
                        <input name="name" type="text" class="form-control" value="{{request()->get('name')}}" style="max-width: 300px; margin-right: 15px" placeholder="Điền tên máy in">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Tìm kiếm</button>
                        <button class="btn btn-info text-white" type="button" data-toggle="modal" data-target="#exampleModalCenter">+Tạo mới</button>
                    </form>

                    <div class="row text-center" style="border: 1px solid #333333">
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Tên máy in</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Tên đơn vị</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Mã UID máy in</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Địa chỉ đặt máy</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Số liên</p>
                        </div>
                        <div class="col-2">
                            <p class="m-0 p-1">***</p>
                        </div>
                    </div>
                    @if($listData->total() > 0)
                        @foreach($listData as $value)
                            <div class="row text-center" style="border: 1px solid #333333">
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->mayin_tenmay}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->donvi_ten}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->mayin_uid}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->mayin_diachi}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->mayin_solien}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center p-2">
                                    <button class="btn btn-primary btn-edit" value="{{$value->id}}" style="margin-right: 10px">Sửa</button>
                                    <a href="{{route('admin.printer.delete',$value->id)}}" class="btn btn-danger btn-delete">Xóa</a>
                                </div>
                            </div>
                        @endforeach
                        <div class="d-flex justify-content-center mt-4">
                            {{ $listData->appends([request()->all()])->links('pagination_custom.index') }}
                        </div>
                    @else
                        <p class="text-danger text-center mt-3">Không có dữ liệu</p>
                    @endif
                </div>
            </div>
            <div class="modal fade" id="modalEditService" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Cập nhật máy in</h5>
                        </div>
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tạo mới dịch vụ</h5>
                            <button type="button" class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="form" action="{{route('admin.printer.create')}}" method="post" role="form" autocomplete="off">
                                @csrf
                                <div class="form-group">
                                    <label for="inputName">Chọn đơn vị</label>
                                    <select class="form-select" id="selectAgency" name="id_donvi" required>
                                        @foreach($listAgency as $agency)
                                            <option value="{{$agency->id}}">{{$agency->donvi_ten}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Tên máy in</label>
                                    <input type="text" class="form-control" required name="name" id="inputName" placeholder="Tên máy in">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3">Mã UID máy in</label>
                                    <input type="text" class="form-control" name="uid_code" id="inputEmail3" placeholder="Mã UID máy in" required="">
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3">Địa chỉ đặt máy	</label>
                                    <input type="text" class="form-control" name="address" id="inputPassword3" placeholder="Địa chỉ đặt máy" required="">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Ghi chú dòng 1</label>
                                    <input type="text" class="form-control" name="line1" id="inputVerify3" placeholder="Ghi chú dòng 1">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify4">Ghi chú dòng 2</label>
                                    <input type="text" class="form-control" name="line2" id="inputVerify4" placeholder="Ghi chú dòng 2">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify5">Số liên</label>
                                    <input type="text" class="form-control validate" name="soline" required placeholder="Số liên">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Bắt đầu sáng</label>
                                    <input type="time" class="form-control" name="startMorning" id="inputVerify3" placeholder="Bắt đầu sáng">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Hết giờ sáng</label>
                                    <input type="time" class="form-control" name="endMorrning" id="inputVerify3" placeholder="Hết giờ sáng">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Bắt đầu chiều</label>
                                    <input type="time" class="form-control" name="startAfternoon" id="inputVerify3" placeholder="Bắt đầu chiều">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Hết giờ chiều</label>
                                    <input type="time" class="form-control" name="endAfternoon" id="inputVerify3" placeholder="Hết giờ chiều">
                                </div>
                                <div class="d-flex justify-content-center mt-3">
                                    <button class="btn btn-danger btn-close-modal" type="button" style="margin-right: 15px">Hủy</button>
                                    <button type="submit" class="btn btn-primary" style="margin-right: 15px">Cập nhật</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @include('admin.layout.footer')
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-delete").click(function (ev) {
                ev.preventDefault();
                let url = $(this).attr('href');
                $.confirm({
                    title: 'Xác nhận!',
                    content: 'Bạn chắc xóa dịch vụ này không?',
                    buttons: {
                        confirm: {
                            text: 'Xác nhận',
                            btnClass: 'btn-blue',
                            action: function(){
                                location.replace(url);
                            }
                        },
                        cancel: {
                            text: 'Hủy',
                            btnClass: 'btn-danger',
                            action: function(){}
                        }
                    }
                });
            });
            $(".btn-edit").click(function () {
                let service_id = $(this).val();
                $.ajax({
                    url: window.location.origin + '/admin/printer/show',
                    data: {'id' : service_id},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.status){
                            $("#modalEditService .modal-body").html(data.html);
                            $("#modalEditService").modal("show");
                        }else{
                            alert(data.msg);
                        }
                    }
                })
            });
            $(document).on("click", ".btn-close-modal", function () {
                $("#modalEditService").modal("hide");
            });
        });
    </script>
@endsection
