<form class="form" action="{{route('admin.printer.update',$printer->id)}}" method="post" role="form" autocomplete="off">
    @csrf
    <div class="form-group">
        <label for="inputName">Tên máy in</label>
        <input type="text" class="form-control" required name="name" value="{{$printer->mayin_tenmay}}" id="inputName" placeholder="Tên máy in">
    </div>
    <div class="form-group">
        <label for="inputEmail3">Mã UID máy in</label>
        <input type="text" class="form-control" name="uid_code" id="inputEmail3" value="{{$printer->mayin_uid}}" placeholder="Mã UID máy in" required="">
    </div>
    <div class="form-group">
        <label for="inputPassword3">Địa chỉ đặt máy	</label>
        <input type="text" class="form-control" name="address" id="inputPassword3" value="{{$printer->mayin_diachi}}" placeholder="Địa chỉ đặt máy" required="">
    </div>
    <div class="form-group">
        <label for="inputVerify3">Ghi chú dòng 1</label>
        <input type="text" class="form-control" name="line1" id="inputVerify3" value="{{$printer->mayin_line1}}" placeholder="Ghi chú dòng 1">
    </div>
    <div class="form-group">
        <label for="inputVerify4">Ghi chú dòng 2</label>
        <input type="text" class="form-control" name="line2" id="inputVerify4" value="{{$printer->mayin_line2}}" placeholder="Ghi chú dòng 2">
    </div>
    <div class="form-group">
        <label for="inputVerify5">Số liên</label>
        <input type="text" class="form-control validate" name="soline" value="{{$printer->mayin_solien}}" required placeholder="Số liên">
    </div>
    <div class="form-group">
        <label for="inputVerify3">Bắt đầu sáng</label>
        <input type="time" class="form-control" name="startMorning" value="{{$printer->mayin_startMorning}}" id="inputVerify3" placeholder="Bắt đầu sáng">
    </div>
    <div class="form-group">
        <label for="inputVerify3">Hết giờ sáng</label>
        <input type="time" class="form-control" name="endMorrning" value="{{$printer->mayin_endMorrning}}" id="inputVerify3" placeholder="Hết giờ sáng">
    </div>
    <div class="form-group">
        <label for="inputVerify3">Bắt đầu chiều</label>
        <input type="time" class="form-control" name="startAfternoon" value="{{$printer->mayin_startAfternoon}}" id="inputVerify3" placeholder="Bắt đầu chiều">
    </div>
    <div class="form-group">
        <label for="inputVerify3">Hết giờ chiều</label>
        <input type="time" class="form-control" name="endAfternoon" value="{{$printer->mayin_startAfternoon}}" id="inputVerify3" placeholder="Hết giờ chiều">
    </div>
    <div class="d-flex justify-content-center mt-3">
        <button class="btn btn-danger btn-close-modal" type="button" style="margin-right: 15px">Hủy</button>
        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Cập nhật</button>
    </div>
</form>
