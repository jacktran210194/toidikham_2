<form action="{{route('admin.user.update',$user->id)}}" method="post">
    @csrf
    <div class="mb-3">
        <label class="mb-2">Tên người dùng</label>
        <input name="user_hoten" maxlength="255" value="{{$user->user_hoten}}" type="text" required class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Số điện thoại</label>
        <input name="username" value="{{$user->username}}" type="text" disabled class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Mật khẩu</label>
        <input name="password" type="password" required class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Email</label>
        <input name="user_email" maxlength="255" value="{{$user->user_email}}" type="email" class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Ghi chú</label>
        <textarea name="user_ghichu" maxlength="255" class="form-control">{{$user->user_ghichu}}</textarea>
    </div>
    <div class="d-flex justify-content-center">
        <button class="btn btn-danger btn-close-modal" type="button" style="margin-right: 15px">Hủy</button>
        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Cập nhật</button>
    </div>
</form>
