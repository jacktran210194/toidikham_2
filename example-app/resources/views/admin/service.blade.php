@extends('admin.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container">
                <div class="p-3">
                    <h1>Dịch vụ</h1>
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form action="{{route('admin.service.index')}}" method="get" class="d-flex align-items-center">
                        <select name="id_donvi" class="form-select" style="max-width: 200px;margin-right: 15px">
                            <option value="">Chọn đơn vị </option>
                            @foreach($listAgency as $agency)
                                <option value="{{$agency->id}}" @if($agency->id == request()->get('id_donvi')) selected @endif>{{$agency->donvi_ten}}</option>
                            @endforeach
                        </select>
                        <input name="name" type="text" class="form-control" value="{{request()->get('name')}}" style="max-width: 300px; margin-right: 15px" placeholder="Điền tên dịch vụ">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Tìm kiếm</button>
                        <button class="btn btn-info text-white" type="button" data-toggle="modal" data-target="#exampleModalCenter">+Tạo mới</button>
                    </form>

                    <div class="row text-center" style="border: 1px solid #333333">
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Tên dịch vụ</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Tên đơn vị</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Số bắt đầu</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Số tối đa</p>
                        </div>
                        <div class="col-2" style="border-right: 1px solid #333333">
                            <p class="m-0 p-1">Mã màu</p>
                        </div>
                        <div class="col-2">
                            <p class="m-0 p-1">***</p>
                        </div>
                    </div>
                    @if($listData->total() > 0)
                        @foreach($listData as $value)
                            <div class="row text-center" style="border: 1px solid #333333">
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->dichvu_ten}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->donvi_ten}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->dichvu_sobatdau}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->dichvu_sotoida}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center" style="border-right: 1px solid #333333">
                                    <p class="m-0 p-1">{{$value->bg_color}}</p>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-center p-2">
                                    <button class="btn btn-primary btn-edit" value="{{$value->id}}" style="margin-right: 10px">Sửa</button>
                                    <a href="{{route('admin.service.delete',$value->id)}}" class="btn btn-danger btn-delete">Xóa</a>
                                </div>
                            </div>
                        @endforeach
                        <div class="d-flex justify-content-center mt-4">
                            {{ $listData->appends([request()->all()])->links('pagination_custom.index') }}
                        </div>
                    @else
                        <p class="text-danger text-center mt-3">Không có dữ liệu</p>
                    @endif
                </div>
            </div>
            <div class="modal fade" id="modalEditService" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Cập nhật dịch vụ</h5>
                        </div>
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tạo mới dịch vụ</h5>
                            <button type="button" class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.service.create')}}" method="post">
                                @csrf
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div class="form-floating mb-3 mb-md-0">
                                            <select class="form-select" id="selectAgency" name="id_donvi" required>
                                                @foreach($listAgency as $agency)
                                                    <option value="{{$agency->id}}">{{$agency->donvi_ten}}</option>
                                                @endforeach
                                            </select>
                                            <label for="selectAgency">Chọn đơn vị</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div class="form-floating mb-3 mb-md-0">
                                            <input class="form-control" id="inputFirstName" type="text" required name="dichvu_ten" placeholder="Nhập tên dịch vụ của bạn" />
                                            <label for="inputFirstName">Tên dịch vụ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input class="form-control validate" id="inputLastName" type="text" name="dichvu_sobatdau" placeholder="Enter your last name" />
                                            <label for="inputLastName">Số bắt đầu</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control validate" id="inputEmail" type="text" name="dichvu_socuoi" placeholder="name@example.com" />
                                    <label for="inputEmail">Số cuối</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control validate" id="max_number" type="text" name="dichvu_sotoida" placeholder="name@example.com" />
                                    <label for="max_number">Số tối đa</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="bg_color" type="color" required name="bg_color" placeholder="name@example.com" />
                                    <label for="bg_color">Màu của dịch vụ</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <textarea class="form-control" id="note" name="dichvu_ghichu" maxlength="250" placeholder="note"></textarea>
                                    <label for="note">Ghi chú</label>
                                </div>
                                <div class="mt-4 mb-0">
                                    <div class="d-flex justify-content-center">
                                        <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close" style="margin-right: 15px">Hủy</button>
                                        <button class="btn btn-primary btn-block" type="submit">Tạo mới dịch vụ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @include('admin.layout.footer')
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-delete").click(function (ev) {
                ev.preventDefault();
                let url = $(this).attr('href');
                $.confirm({
                    title: 'Xác nhận!',
                    content: 'Bạn chắc xóa dịch vụ này không?',
                    buttons: {
                        confirm: {
                            text: 'Xác nhận',
                            btnClass: 'btn-blue',
                            action: function(){
                                location.replace(url);
                            }
                        },
                        cancel: {
                            text: 'Hủy',
                            btnClass: 'btn-danger',
                            action: function(){}
                        }
                    }
                });
            });
            $(".btn-edit").click(function () {
                let service_id = $(this).val();
                $.ajax({
                    url: window.location.origin + '/admin/service/show',
                    data: {'id' : service_id},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.status){
                            $("#modalEditService .modal-body").html(data.html);
                            $("#modalEditService").modal("show");
                        }else{
                            alert(data.msg);
                        }
                    }
                })
            });
            $(document).on("click", ".btn-close-modal", function () {
                $("#modalEditService").modal("hide");
            });
        });
    </script>
@endsection
