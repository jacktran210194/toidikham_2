@extends('agency.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<style>
    .popup-content{
        top: 0;
        left: 0;
        background: #ffffff30;
        opacity: 0;
        z-index: -10;
        transition: .1s;
    }
    .card-hover:hover .popup-content{
        opacity: 1;
        z-index: 10;
    }
</style>
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4 mb-4">Danh sách khách hàng</h1>
                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-table me-1"></i>
                            Danh sách khách hàng
                        </div>
                        <div class="d-flex align-items-center justify-content-end w-50">
                            <form action="{{route('agency.index')}}" method="get" class="d-flex align-items-center justify-content-end mb-0">
                                <input name="date_form" value="{{request()->get('date_form')}}" type="date" class="form-control" style="max-width: 150px;margin-right: 15px">
                                <input name="date_to" value="{{request()->get('date_to')}}" type="date" class="form-control" style="max-width: 150px;margin-right: 15px">
                                <button class="btn btn-primary" type="submit" style="margin-right: 15px">Tìm kiếm</button>
                                <a href="{{url('don-vi/xuat-excel?date_form='.request()->get('date_form').'&date_to='.request()->get('date_to'))}}" class="btn btn-info text-white">Xuất Excel</a>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Dịch vụ</th>
                                <th>Số điện thoại</th>
                                <th>STT</th>
                                <th>Ngày giờ</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Tên</th>
                                <th>Dịch vụ</th>
                                <th>Số điện thoại</th>
                                <th>STT</th>
                                <th>Ngày giờ</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($listUser as $value )
                                <tr>
                                    <td>{{$value->khach_tenkhach}}</td>
                                    <td>{{$value->ten_dichvu}}</td>
                                    <td>{{$value->khach_dienthoai}}</td>
                                    <td>{{$value->khach_sothutu}}</td>
                                    <td>{{date_format(date_create($value->khach_giolayso), 'd/m/Y')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        @include('agency.layout.footer')
    </div>
@stop
