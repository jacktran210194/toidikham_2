@extends('agency.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4 mb-4">{{$service->dichvu_ten}}</h1>
                <form action="{{route('agency.service.update',$service->id)}}" method="post" class="card-body border p-3 mb-4">
                    @csrf
                    <input hidden value="{{$service->id}}" name="service_id">
                    <h3 class="text-center">Thông tin dịch vụ</h3>
                    <div class="mb-3">
                        <label class="mb-2">Tên dịch vụ</label>
                        <input name="dichvu_ten" value="{{$service->dichvu_ten}}" type="text" required class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="mb-2">Số bắt đầu</label>
                        <input name="dichvu_sobatdau" value="{{$service->dichvu_sobatdau}}" type="number" required class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="mb-2">Số cuối</label>
                        <input name="dichvu_socuoi" value="{{$service->dichvu_socuoi}}" type="number" required class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="mb-2">Số tối đa</label>
                        <input name="dichvu_sotoida" value="{{$service->dichvu_sotoida}}" type="number" required class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="mb-2">ID dịch vụ</label>
                        <input name="id_local" value="{{$service->id_localservice}}" type="number" required class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="mb-2">Màu sắc</label>
                        <input name="bg_color" value="{{$service->bg_color}}" type="color" required class="form-control">
                    </div>
                    <div class="mb-3">
                        <label class="mb-2">Ghi chú</label>
                        <textarea name="dichvu_ghichu" class="form-control">{{$service->dichvu_ghichu}}</textarea>
                    </div>
                    <div class="mb-3">
                        <label class="mb-2">Bật / Tắt</label>
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" name="active" @if($service->dichvu_isActive) checked @endif>
                            <label class="form-check-label" for="flexSwitchCheckChecked">@if($service->dichvu_isActive) Dịch vụ đang hoạt động @else Dịch vụ tạm tắt @endif</label>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Cập nhật</button>
                    </div>
                </form>

                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-table me-1"></i>
                            Danh sách người dùng lấy số thứ tự
                        </div>
                       <div class="d-flex align-items-center justify-content-end w-50">
                           <form action="{{route('agency.service.details',$service->id)}}" method="get" class="d-flex align-items-center justify-content-end mb-0">
                               <input name="date_form" value="{{request()->get('date_form')}}" type="date" class="form-control" style="max-width: 150px;margin-right: 15px">
                               <input name="date_to" value="{{request()->get('date_to')}}" type="date" class="form-control" style="max-width: 150px;margin-right: 15px">
                               <button class="btn btn-primary" type="submit" style="margin-right: 15px">Tìm kiếm</button>
                               <a href="{{url('don-vi/dich-vu/xuat-excel/'.$service->id.'?date_form='.request()->get('date_form').'&date_to='.request()->get('date_to'))}}" class="btn btn-info text-white">Xuất Excel</a>
                           </form>
                       </div>
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Dịch vụ</th>
                                <th>Số điện thoại</th>
                                <th>STT</th>
                                <th>Ngày giờ</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Tên</th>
                                <th>Dịch vụ</th>
                                <th>Số điện thoại</th>
                                <th>STT</th>
                                <th>Ngày giờ</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($listUser as $value )
                            <tr>
                                <td><p style="max-width: 200px">{{$value->khach_tenkhach}}</p></td>
                                <td>{{$value->ten_dichvu}}</td>
                                <td><p style="max-width: 200px">{{$value->khach_dienthoai}}</p></td>
                                <td>{{$value->khach_sothutu}}</td>
                                <td>{{date_format(date_create($value->khach_giolayso), 'd/m/Y')}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        @include('agency.layout.footer')
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $(".btn-delete").click(function () {
            let service_id = $('input[name="service_id"]').val();
            let url = window.location.origin + '/don-vi/dich-vu/delete/'+service_id;
            $.confirm({
                title: 'Xác nhận!',
                content: 'Bạn chắc xóa dịch vụ này không?',
                buttons: {
                    confirm: {
                        text: 'Xác nhận',
                        btnClass: 'btn-blue',
                        action: function(){
                            location.replace(url);
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        btnClass: 'btn-danger',
                        action: function(){}
                    }
                }
            });
        });
    </script>
@endsection
