@extends('agency.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<style>
    .popup-content{
        top: 0;
        left: 0;
        background: #ffffff30;
        opacity: 0;
        z-index: -10;
        transition: .1s;
    }
    .card-hover:hover .popup-content{
        opacity: 1;
        z-index: 10;
    }
</style>
@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="mt-4 mb-4">Danh sách dịch vụ</h1>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">+ Thêm dịch vụ</button>
                </div>
                <div class="row">
                    @foreach($service as $_item)
                        <div class="col-xl-3 col-md-6">
                            <div class="card text-white mb-4 position-relative card-hover" style="background: @if(isset($_item->bg_color)) {{$_item->bg_color}} @else #0a53be @endif">
                                <div class="card-body">{{$_item->dichvu_ten}}</div>
                                <div class="card-body d-flex justify-content-between pt-0" style="font-size: 14px">
                                    <p class="m-0">Tổng người lấy stt :</p>
                                    <p class="m-0">{{$_item->total}}</p>
                                </div>
                                <div class="card-footer d-flex align-items-center justify-content-between">
                                    <a class="small text-white stretched-link" href="{{route('agency.service.details',$_item->id)}}">Xem chi tiết</a>
                                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                </div>
                                <div class="position-absolute w-100 h-100 d-flex justify-content-center align-items-center popup-content">
                                    <a class="btn btn-primary" href="{{route('agency.service.details',$_item->id)}}" style="margin-right: 5px">Xem</a>
                                    <button class="btn btn-warning btn-edit-service text-white" value="{{$_item->id}}" type="button" style="margin-right: 5px">Sửa</button>
                                    <button type="button" class="btn btn-danger btn-delete" value="{{$_item->id}}">Xóa</button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tạo mới dịch vụ</h5>
                            <button type="button" class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('agency.service.create')}}" method="post">
                                @csrf
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div class="form-floating mb-3 mb-md-0">
                                            <input class="form-control" id="inputFirstName" type="text" required name="dichvu_ten" placeholder="Nhập tên dịch vụ của bạn" />
                                            <label for="inputFirstName">Tên dịch vụ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input class="form-control validate" id="inputLastName" type="text" name="dichvu_sobatdau" placeholder="Enter your last name" />
                                            <label for="inputLastName">Số bắt đầu</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control validate" id="inputEmail" type="text" name="dichvu_socuoi" placeholder="name@example.com" />
                                    <label for="inputEmail">Số cuối</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control validate" id="max_number" type="text" name="dichvu_sotoida" placeholder="name@example.com" />
                                    <label for="max_number">Số tối đa</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control validate" id="max_number" type="text"  name="id_local" placeholder="name@example.com" />
                                    <label for="max_number">ID dịch vụ</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" id="bg_color" type="color" required name="bg_color" placeholder="name@example.com" />
                                    <label for="bg_color">Màu của dịch vụ</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <textarea class="form-control" id="note" name="dichvu_ghichu" maxlength="250" placeholder="note"></textarea>
                                    <label for="note">Ghi chú</label>
                                </div>
                                <div class="mt-4 mb-0">
                                    <div class="d-flex justify-content-center">
                                        <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close" style="margin-right: 15px">Hủy</button>
                                        <button class="btn btn-primary btn-block" type="submit">Tạo mới dịch vụ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalEditService" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Sửa dịch vụ</h5>
                        </div>
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>
        </main>
        @include('agency.layout.footer')
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="assets/js/index.js"></script>
@endsection

