@extends('agency.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <section style="background-color: #eee;">
            <div class="container py-5">
                <div class="container py-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 offset-md-3">
                                    <span class="anchor" id="formChangePassword"></span>
                                    <!-- form card change password -->
                                    <div class="card card-outline-secondary">
                                        <div class="card-header">
                                            <h3 class="mb-0">Thay đổi mật khẩu</h3>
                                        </div>
                                        <div class="card-body">
                                            @if(session()->has('success'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('success') }}
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert alert-danger">
                                                    {{ session()->get('error') }}
                                                </div>
                                            @endif
                                            <form class="form" action="{{route('agency.account.update_password')}}" method="post" role="form" autocomplete="off">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="inputPasswordOld">Mật khẩu cũ</label>
                                                    <input type="password" name="password" class="form-control" id="inputPasswordOld" required="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPasswordNew">Mật khẩu mới</label>
                                                    <input type="password" name="new_password" class="form-control" id="inputPasswordNew" required="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPasswordNewVerify">Nhập lại mật khẩu mới</label>
                                                    <input type="password" name="confirm_password" class="form-control" id="inputPasswordNewVerify" required="">
                                                    <span class="form-text small text-muted"> Để xác nhận, hãy nhập lại mật khẩu mới </span>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success btn-lg float-right">Xác nhận</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/col-->
                    </div>
                </div>
                <!--/container-->
            </div>
        </section>
        @include('agency.layout.footer')
    </div>
@stop

