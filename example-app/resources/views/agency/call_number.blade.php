@extends('agency.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<style>
    .text-ellipsis{
        display: block;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        width: 100%;
    }
</style>
@section('content')
    <div id="layoutSidenav_content">
        <div class="p-3">
            <div class="row">
                <div class="col-12">
                    <p class="text-center mb-2 text-danger" style="font-weight: bold">Chọn dịch vụ bạn muốn gọi ở phần bên dưới</p>
                </div>
                @foreach($listService as $_service)
                    <div class="col-lg-3 col-6 mb-2">
                        <a href="{{route('agency.call.service',$_service->id)}}" class="btn w-100 @if($_service->id == $service->id) text-danger @else text-white @endif" style="background: {{$_service->bg_color}};font-weight: bold">{{$_service->dichvu_ten}}</a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="p-3">
            <div class="row">
                @if(isset($is_call))
                    <div class="col-xl-3 col-md-6">
                        <div class="card text-white mb-4 bg-primary">
                            <div class="p-2">Dịch vụ: {{$is_call->ten_dichvu}}</div>
                            <div class="p-2">Tên khách hàng: {{$is_call->khach_tenkhach}}</div>
                            <div class="p-2">Số điện thoại: {{$is_call->khach_dienthoai}}</div>
                            <div class="p-2">Số thứ tự: {{$is_call->khach_sothutu}}</div>
                        </div>
                    </div>
                @endif
                @foreach($listCustomer as $list)
                    <div class="col-xl-3 col-md-6">
                        <div class="card text-white mb-4 bg-warning">
                            <div class="p-2">Dịch vụ: {{$list->ten_dichvu}}</div>
                            <div class="p-2">Tên khách hàng: {{$list->khach_tenkhach}}</div>
                            <div class="p-2">Số điện thoại: {{$list->khach_dienthoai}}</div>
                            <div class="p-2">Số thứ tự: {{$list->khach_sothutu}}</div>
                        </div>
                    </div>
                @endforeach
            </div>
            @if(count($listCustomer))
                <div class="p-2 d-flex justify-content-center">
                    <button class="btn btn-primary w-25 btn-call" style="font-weight: bold;font-size: 18px">Gọi số</button>
                </div>
                @else
                <div class="p-2 d-flex justify-content-center">
                    <button class="btn btn-danger w-25" style="font-weight: bold;font-size: 18px">Không có số</button>
                </div>
            @endif
        </div>
        <div class="p-3">
            <h3 class="text-center">Danh sách số thứ tự trong ngày</h3>
            @if(count($listAllCustomer))
                <div class="row" style="border: 1px solid #333333">
                    <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">Tên</p></div>
                    <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">Số điện thoại</p></div>
                    <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">STT</p></div>
                    <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">Thời gian lấy số</p></div>
                    <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">Thời gian gọi</p></div>
                    <div class="col-2 p-0"><p class="m-0 p-2">Thời gian kết thúc</p></div>
                </div>
                @foreach($listAllCustomer as $value)
                    <div class="row @if($value->khach_isActive == 1) bg-primary @endif" style="border: 1px solid #333333;">
                        <div class="col-2 p-0 d-flex" style="border-right: 1px solid #333333;"><p class="m-0 p-2" style="overflow-wrap: anywhere;">{{$value->khach_tenkhach}}</p></div>
                        <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">{{$value->khach_dienthoai}}</p></div>
                        <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">{{$value->khach_sothutu}}</p></div>
                        <div class="col-2 p-0" style="border-right: 1px solid #333333"><p class="m-0 p-2">{{date_format(date_create($value->khach_giolayso), 'H:i d-m-Y')}}</p></div>
                        <div class="col-2 p-0" style="border-right: 1px solid #333333">
                            <p class="m-0 p-2">
                               @if(isset($value->khach_giogoiso))
                                    {{date_format(date_create($value->khach_giogoiso), 'H:i d-m-Y')}}
                               @endif
                            </p>
                        </div>
                        <div class="col-2 p-0"><p class="m-0 p-2">
                                @if(isset($value->khach_gioketthuc))
                                    {{date_format(date_create($value->khach_gioketthuc), 'H:i d-m-Y')}}
                                @endif
                            </p></div>
                    </div>
                @endforeach
                @else
                <p class="text-danger">Không có khách hàng nào đặt số</p>
            @endif
        </div>
        @include('agency.layout.footer')
    </div>
@stop
@section('script')
    <script>
        $(".btn-call").click(function () {
            var form = new FormData();
            form.append("token", "{{$agency->token}}");
            form.append("id_dichvu", "{{$service->id}}");

            var settings = {
                "url": window.location.origin + '/api/goi-so-thu-tu',
                "method": "POST",
                "timeout": 0,
                "processData": false,
                "mimeType": "multipart/form-data",
                "contentType": false,
                "data": form
            };

            $.ajax(settings).done(function (response) {
                var data = JSON.parse(response);
                if (data.status){
                    location.reload();
                }else {
                    alert(data.msg);
                }
            });
        });
    </script>
@endsection
