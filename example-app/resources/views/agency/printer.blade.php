@extends('agency.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <section class="bg-white">
            <div class="container py-5">
                <h1>Danh sách máy in</h1>
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                @if(count($listPrinter))
                <div class="row" style="border: 1px solid #333333">
                    <div class="col-2" style="border-right: 1px solid #333333">
                        <p class="m-0 p-1 text-center">Tên máy in</p>
                    </div>
                    <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">Bắt đầu sáng</p></div>
                    <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">Hết giờ sáng</p></div>
                    <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">Bắt đầu chiều</p></div>
                    <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">Hết giờ chiều</p></div>
                    <div class="col-2"><p class="m-0 p-1 text-center">Trạng thái</p></div>
                </div>
                    @foreach($listPrinter as $value)
                        <div class="row" style="border: 1px solid #333333">
                            <div class="col-2" style="border-right: 1px solid #333333">
                                <p class="m-0 p-1 text-center">{{$value->mayin_tenmay}}</p>
                            </div>
                            <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">{{$value->mayin_startMorning}}</p></div>
                            <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">{{$value->mayin_endMorrning}}</p></div>
                            <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">{{$value->mayin_startAfternoon}}</p></div>
                            <div class="col-2" style="border-right: 1px solid #333333"><p class="m-0 p-1 text-center">{{$value->mayin_endAfternoon}}</p></div>
                            <div class="col-2">
                                <div class="p-1 d-flex justify-content-center align-items-center">
                                    <button class="btn btn-warning text-white btn-edit" value="{{$value->id}}" style="margin-right: 10px">Sửa</button>
                                    <a class="btn btn-danger text-white btn-delete" href="{{route('agency.printer.delete',$value->id)}}">Xóa</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p class="text-danger text-center">Không có dữ liệu</p>
                @endif
            </div>
            <div class="modal fade" id="modalEditPrinter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Cập nhật máy in</h5>
                        </div>
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>
        </section>
        @include('agency.layout.footer')
    </div>
@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".btn-edit").click(function () {
            $.ajax({
                url: window.location.origin + '/don-vi/may-in/thong-tin',
                type: 'post',
                data: {'id' : $(this).val()},
                dataType: 'json',
                success: function (data) {
                    if (data.status){
                        $("#modalEditPrinter .modal-body").html(data.html);
                        $("#modalEditPrinter").modal("show");
                    }else{
                        alert(data.msg)
                    }
                }
            })
        });
        $(".btn-delete").click(function (ev) {
            ev.preventDefault();
            let url = $(this).attr('href');
            $.confirm({
                title: 'Xác nhận!',
                content: 'Bạn chắc xóa dịch vụ này không?',
                buttons: {
                    confirm: {
                        text: 'Xác nhận',
                        btnClass: 'btn-blue',
                        action: function(){
                            location.replace(url);
                        }
                    },
                    cancel: {
                        text: 'Hủy',
                        btnClass: 'btn-danger',
                        action: function(){}
                    }
                }
            });
        });
        $(document).on("click", ".btn-close-modal", function () {
            $("#modalEditPrinter").modal("hide");
        });
    </script>
@endsection
