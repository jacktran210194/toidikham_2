@extends('agency.layout.index')
<style>
    .form-group{
        margin-bottom: 15px;
    }
</style>
@section('content')
    <div id="layoutSidenav_content">
        <section style="background-color: #eee;">
            <div class="container py-5">
                <!--/container-->
                <div class="col-md-6 offset-md-3">
                    <!-- form card register -->
                    <div class="card card-outline-secondary">
                        <div class="card-header">
                            <h3 class="mb-0">Thêm mới máy in</h3>
                        </div>
                        <div class="card-body">
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <form class="form" action="{{route('agency.printer.store')}}" method="post" role="form" autocomplete="off">
                                @csrf
                                <div class="form-group">
                                    <label for="inputName">Tên máy in</label>
                                    <input type="text" class="form-control" required name="name" id="inputName" placeholder="Tên máy in">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3">Mã UID máy in</label>
                                    <input type="text" class="form-control" name="uid_code" id="inputEmail3" placeholder="Mã UID máy in" required="">
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3">Địa chỉ đặt máy	</label>
                                    <input type="text" class="form-control" name="address" id="inputPassword3" placeholder="Địa chỉ đặt máy" required="">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Ghi chú dòng 1</label>
                                    <input type="text" class="form-control" name="line1" id="inputVerify3" placeholder="Ghi chú dòng 1">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify4">Ghi chú dòng 2</label>
                                    <input type="text" class="form-control" name="line2" id="inputVerify4" placeholder="Ghi chú dòng 2">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify5">Số liên</label>
                                    <input type="text" class="form-control validate" name="soline" required placeholder="Số liên">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Bắt đầu sáng</label>
                                    <input type="time" class="form-control" name="startMorning" id="inputVerify3" placeholder="Bắt đầu sáng">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Hết giờ sáng</label>
                                    <input type="time" class="form-control" name="endMorrning" id="inputVerify3" placeholder="Hết giờ sáng">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Bắt đầu chiều</label>
                                    <input type="time" class="form-control" name="startAfternoon" id="inputVerify3" placeholder="Bắt đầu chiều">
                                </div>
                                <div class="form-group">
                                    <label for="inputVerify3">Hết giờ chiều</label>
                                    <input type="time" class="form-control" name="endAfternoon" id="inputVerify3" placeholder="Hết giờ chiều">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success float-right">Tạo mới</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /form card register -->

                </div>
            </div>
        </section>
        @include('agency.layout.footer')
    </div>
@stop
