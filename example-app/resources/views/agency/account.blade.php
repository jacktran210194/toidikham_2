@extends('agency.layout.index')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@section('content')
    <div id="layoutSidenav_content">
        <section style="background-color: #eee;">
            <div class="container py-5">
                <form action="{{route('agency.account.update')}}" method="post" class="row justify-content-center">
                    @csrf
                    <div class="col-lg-8">
                        <div class="card mb-4">
                            <div class="card-body">
                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Tên đơn vị</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="border-0 w-100 text-muted form-control" name="donvi_ten" type="text"
                                               value="{{$agency->donvi_ten}}" required placeholder="Vui lòng nhập tên đơn vị">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Email</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="border-0 w-100 text-muted form-control" name="user_email" type="email"
                                               value="{{$agency->user_email}}" required placeholder="example@example.com">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Số điện thoại</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="text-muted mb-0 border-0 form-control">{{$agency->username}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p class="mb-0">Địa chỉ</p>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="border-0 w-100 text-muted form-control" name="donvi_diachi" type="text"
                                               value="{{$agency->donvi_diachi}}" required placeholder="Nhập địa chỉ">
                                    </div>
                                </div>
                                    <hr>
                                <div class="row justify-content-center d-flex">
                                    <button type="submit" class="btn btn-primary w-50">Cập nhật</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        @include('agency.layout.footer')
    </div>
@stop

