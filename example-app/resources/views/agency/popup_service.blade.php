<form action="{{route('agency.service.update',$service->id)}}" method="post">
    @csrf
    <input hidden value="{{$service->id}}" name="service_id">
    <h3 class="text-center">{{$service->dichvu_ten}}</h3>
    <div class="mb-3">
        <label class="mb-2">Tên dịch vụ</label>
        <input name="dichvu_ten" value="{{$service->dichvu_ten}}" type="text" required class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Số bắt đầu</label>
        <input name="dichvu_sobatdau" maxlength="4" value="{{$service->dichvu_sobatdau}}" type="text"  class="form-control validate">
    </div>
    <div class="mb-3">
        <label class="mb-2">Số cuối</label>
        <input name="dichvu_socuoi" value="{{$service->dichvu_socuoi}}" type="text"  class="form-control validate">
    </div>
    <div class="mb-3">
        <label class="mb-2">Số tối đa</label>
        <input name="dichvu_sotoida" value="{{$service->dichvu_sotoida}}" type="text"  class="form-control validate">
    </div>
    <div class="form-floating mb-3">
        <input class="form-control validate" id="max_number" type="text"  name="id_local" value="{{$service->id_localservice}}" placeholder="name@example.com" />
        <label for="max_number">ID dịch vụ</label>
    </div>
    <div class="mb-3">
        <label class="mb-2">Màu sắc</label>
        <input name="bg_color" value="{{$service->bg_color}}" type="color" required class="form-control">
    </div>
    <div class="mb-3">
        <label class="mb-2">Ghi chú</label>
        <textarea name="dichvu_ghichu" maxlength="250" class="form-control">{{$service->dichvu_ghichu}}</textarea>
    </div>
    <div class="mb-3">
        <label class="mb-2">Bật / Tắt</label>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" name="active" @if($service->dichvu_isActive) checked @endif>
            <label class="form-check-label" for="flexSwitchCheckChecked">@if($service->dichvu_isActive) Dịch vụ đang hoạt động @else Dịch vụ tạm tắt @endif</label>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <button class="btn btn-danger btn-close-modal" type="button" style="margin-right: 15px">Hủy</button>
        <button type="submit" class="btn btn-primary" style="margin-right: 15px">Cập nhật</button>
    </div>
</form>
