<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use App\Models\DonviModel;
use App\Models\PrinterModel;
use App\Models\ServiceModel;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getUserIDAgency ()
    {
        $user_id = Auth::guard('agency')->id();
        return $user_id;
    }

    /**
     * Xuất excel
    **/
    public function exportExcel ($listData, $file_name, $title)
    {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($title);

        $sheet->setCellValue('A1', 'Tên khách hàng');
        $sheet->setCellValue('B1', 'Số điện thoại');
        $sheet->setCellValue('C1', 'Dịch vụ');
        $sheet->setCellValue('D1', 'STT');
        $sheet->setCellValue('E1', 'Ngày giờ lấy số ');
        $sheet->setCellValue('F1', 'Ngày giờ gọi số ');
        $sheet->setCellValue('G1', 'Ngày giờ kết thúc');
        $sheet->setCellValue('H1', 'Thời gian chờ');
        $sheet->setCellValue('I1', 'Thời gian phục vụ');
        $sheet->setCellValue('J1', 'Máy in');
        $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','I','J'];
        foreach ($columns as $index => $column) {
            $columnDimension = $sheet->getColumnDimension($column);
            $columnDimension->setWidth(30);
        }

        foreach ($listData as $key => $value) {
            $service = ServiceModel::find($value->id_dichvu);
            $printer = PrinterModel::find($value->id_mayin);
            $row = $key + 2;
            $sheet->setCellValue('A'.$row, $value->khach_tenkhach);
            $sheet->setCellValue('B'.$row, $value->khach_dienthoai);
            $sheet->setCellValue('C'.$row, isset($service) ? $service->dichvu_ten :$value->ten_dichvu);
            $sheet->setCellValue('D'.$row, $value->khach_sothutu);
            $sheet->setCellValue('E'.$row, date_format(date_create($value->khach_giolayso), 'H:i d/m/Y'));
            if (isset($value->khach_giogoiso)){
                $sheet->setCellValue('F'.$row, date_format(date_create($value->khach_giogoiso), 'H:i d/m/Y'));
            }
            if (isset($value->khach_gioketthuc)){
                $sheet->setCellValue('G'.$row, date_format(date_create($value->khach_gioketthuc), 'H:i d/m/Y'));
            }
            $sheet->setCellValue('H'.$row, $value->khach_thoigiancho);
            $sheet->setCellValue('I'.$row, $value->khach_thoigianphucvu);
            if (isset($printer)){
                $sheet->setCellValue('J'.$row, $printer->mayin_tenmay);
            }
        }

        $writer = new Xlsx($spreadsheet);

        $filename = $file_name.'.xlsx';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    /**
     * Lấy các ngày của tháng hiện tại
     **/
    public function getDay ()
    {
        $month = date('m');
        $year = date('Y');
        $days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $dates = array();
        $dataDate = array();
        for ($day = 1; $day <= $days_in_month; $day++) {
            $date = date('d-m-Y', strtotime("$year-$month-$day"));
            if ($date == date('d-m-Y')){
                $dataDate[] = date('Y-m-d', strtotime("$year-$month-$day"));
                $dates[] = $date;
                break;
            }
            $dates[] = $date;
            $dataDate[] = date('Y-m-d', strtotime("$year-$month-$day"));
        }
        $dataReturn = [
            'dates' => $dates,
            'dataDate' => $dataDate
        ];
        return $dataReturn;
    }

    /**
     * Viết nội dung khi người dùng quét mã qr
     **/
    protected function render_text_qrcode ($customer)
    {
        $text = 'Tên người đặt: '. $customer->khach_tenkhach.' | Số điện thoại: '.$customer->khach_dienthoai.' | Đơn vị: '.$customer->ten_donvi. ' | Dịch Vụ : '.$customer->ten_dichvu. ' | STT: '.$customer->khach_sothutu.' | Mã: '.$customer->khach_madatso;
        return $text;
    }

    /**
     * Lấy số cuối của dịch vụ và cập nhật số cuối
     **/
    protected function checkNumberLastOfService ($service)
    {
        $number = $service->dichvu_socuoi + 1;
        $service->dichvu_socuoi = $number;
        if (isset($service->dichvu_sotoida)){
            if ($number > $service->dichvu_sotoida){
                $data['status'] = false;
                $data['msg'] = 'Đã vượt quá số lượng đặt số thứ tự trong ngày hôm nay. Vui lòng chờ đến ngày hôm sau. Xin chân thành cảm ơn';
                return $data;
            }
        }
//        $service->thoigian_capnhat = Carbon::now('Asia/Ho_Chi_Minh');
        $service->save();
        $data['status'] = true;
        $data['number'] = $number;
        return $data;
    }
    /**
     * Kiểm tra người dùng đã lấy số thứ tự hay chưa
     **/
    public function checkUserGetNumber($phone, $service_id)
    {
        $user = CustomerModel::where('khach_dienthoai', $phone)->where('id_dichvu', $service_id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->first();
        if (isset($user)){
            return false;
        }else{
            return true;
        }
    }
}
