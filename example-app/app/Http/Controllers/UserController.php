<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use App\Models\DonviModel;
use App\Models\PrinterModel;
use App\Models\ServiceModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function account ()
    {
        $titlePage = 'Đơn Vị - Tài Khoản';
        $namePage = 'account';
        $subName = 'index';
        $agency = DonviModel::join('user', 'user.id', '=', 'donvi.user_id')->select('donvi.*', 'user.user_email', 'user.username')
            ->where('user.id', $this->getUserIDAgency())->first();
        return view('agency.account', compact('titlePage', 'namePage', 'agency', 'subName'));
    }

    public function update (Request $request)
    {
        try{
            $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
            DonviModel::updateAgency($agency, $request->get('donvi_ten'), $request->get('donvi_diachi'),$request->get('user_email'));
            return back()->with(['success' => 'Cập nhật thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Đổi mật khẩu
    **/
    public function password ()
    {
        $titlePage = 'Đơn Vị - Tài Khoản';
        $namePage = 'account';
        $subName = 'password';
        $agency = DonviModel::join('user', 'user.id', '=', 'donvi.user_id')->select('donvi.*', 'user.user_email', 'user.username')
            ->where('user.id', $this->getUserIDAgency())->first();
        return view('agency.account_password', compact('titlePage', 'namePage', 'agency', 'subName'));
    }

    public function updatePassword (Request $request)
    {
        $user = User::find($this->getUserIDAgency());
        $arr = [
            'username' => trim($user->username),
            'password' => trim($request->get('password')),
            'user_phanquyen' => 2
        ];
        if(Auth::guard('agency')->attempt($arr)){ // Kiểm tra mật khẩu cũ có chính xác hay không
            if ($request->get('new_password') != $request->get('confirm_password')){
                return back()->with(['error' => 'Xác nhận lại mật khẩu không chính xác']);
            }
            $user->password = bcrypt($request->get('new_password'));
            $user->save();
            return back()->with(['success' => 'Cập nhật mật khẩu mới thành công']);
        }else{
            return back()->with(['error' => 'Mật khẩu cũ không chính xác']);
        }
    }

    /**
     * Danh sách khách hàng của đơn vị
    **/
    public function customer (Request $request)
    {
        $titlePage = 'Đơn Vị - Khách Hàng';
        $namePage = 'service';
        $subName = 'customer';
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        // Lấy danh sách người dùng đặt stt //
        $listUser = CustomerModel::query();
        if (isset($request->date_form) && isset($request->date_to)){
            $listUser = $listUser->whereDate('khach_giolayso', '>=', $request->get('date_form'))
                ->whereDate('khach_giolayso', '<=', $request->get('date_to'));
        }
        $listUser = $listUser->where('id_donvi', $agency->id)->orderBy('created_at', 'desc')->get();
        return view('agency.customer', compact('titlePage', 'namePage', 'subName', 'listUser'));
    }
    /**
     * Danh sách máy in
    **/
    public function printer ()
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $listPrinter = PrinterModel::listPrinter($agency->id);
        $titlePage = 'Đơn Vị - Máy In';
        $namePage = 'printer';
        $subName = 'index';
        return view('agency.printer', compact('listPrinter', 'titlePage', 'namePage', 'subName'));
    }
    /**
     * Thêm mới máy in
    **/
    public function createPrinter ()
    {
        $titlePage = 'Đơn Vị - Máy In';
        $namePage = 'printer';
        $subName = 'create';
        return view('agency.createPrinter', compact('titlePage', 'namePage', 'subName'));
    }

    public function storePrinter (Request $request)
    {
        try{
            $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
            PrinterModel::createPrinter($agency->id,$request->name, $request->uid_code,$request->address,$request->line1,$request->line2,1,
                $request->soline,1,$request->startMorning,$request->endMorrning,$request->startAfternoon,$request->endAfternoon);
            return back()->with(['success' => 'Tạo mới máy in thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Xóa máy in
    **/
    public function deletePrinter ($id)
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $printer = PrinterModel::where('id', $id)->where('id_donvi', $agency->id)->first();
        if (empty($printer)){
            return back()->with(['error' => 'Máy in không tồn tại']);
        }
        $printer->delete();
        return back()->with(['success' => 'Xóa thành công']);
    }
    /**
     * Thông tin máy in
    **/
    public function infoPrinter (Request $request)
    {
        $printer = PrinterModel::join('donvi', 'donvi.id', '=', 'mayin.id_donvi')->select('mayin.*')
            ->where('donvi.user_id', $this->getUserIDAgency())->where('mayin.id', $request->get('id'))->first();
        if (empty($printer)){
            $data['status'] = false;
            $data['msg'] = 'Không tìm thấy dữ liệu';
            return $data;
        }
        $view = view('agency.printer_details', compact('printer'))->render();
        return response()->json(['status' => true, 'html' => $view]);
    }
    /**
     * Cập nhật máy in
    **/
    public function updatePrinter (Request $request, $id)
    {
       try{
           $printer = PrinterModel::join('donvi', 'donvi.id', '=', 'mayin.id_donvi')->select('mayin.*')
               ->where('donvi.user_id', $this->getUserIDAgency())->where('mayin.id', $id)->first();
           if (empty($printer)){
               return back()->with(['error' => 'Không tìm thấy máy in']);
           }
           PrinterModel::updatePrinter($printer,$request->name,$request->uid_code,$request->address,$request->line1,$request->line2,1,$request->soline,
               1,$request->startMorning,$request->endMorrning,$request->startAfternoon,$request->endAfternoon);
           return back()->with(['success' => 'Cập nhật thành công']);
       }catch (\Exception $exception){
           return back()->with(['error' => $exception->getMessage()]);
       }
    }
}
