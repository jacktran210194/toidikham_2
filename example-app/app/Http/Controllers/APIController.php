<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use App\Models\DonviModel;
use App\Models\PrinterModel;
use App\Models\ServiceModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Exception;


class APIController extends Controller
{
    public function getSTT (Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'id_donvi' => 'required',
                'phone' => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:10|max:10',
                'id_dichvu' => 'required',
            ], [
                'id_donvi.required' => 'Vui lòng chọn đơn vị',
                'phone.required' => 'Vui lòng điền số điện thoại người dùng',
                'phone.regex' => 'Số điện thoại không đúng',
                'id_dichvu.required' => 'Vui lòng chọn dịch vụ',
            ]);
            if ($validator->fails()) {
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $validator->errors()->first();
                return \response()->json($dataReturn, Response::HTTP_OK);
            }
            $agency = DonviModel::find($request->get('id_donvi'));
            $service = ServiceModel::where('id_donvi', $request->get('id_donvi'))->where('id', $request->get('id_dichvu'))->first();
            if (empty($agency) || empty($service) || empty($request->phone)){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Dữ liệu chuyền không chính xác',
                ];
                return \response()->json($dataReturn, Response::HTTP_OK);
            }
            if (!$this->checkUserGetNumber($request->get('phone'), $service->id)){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Số điện thoại này đã được lấy stt trước đó. Vui lòng kiểm tra lại',
                ];
                return \response()->json($dataReturn, Response::HTTP_OK);
            }
            $number = $this->checkNumberLastOfService($service);
            if (!$number['status']){
                return \response()->json($number, Response::HTTP_OK);
            }
            $stt = $service->dichvu_sobatdau+$number['number'];
            $customer = CustomerModel::createCustomer(null, $agency, $service, $stt, $request->get('name'), $request->get('phone'));
            $dataReturn = [
                'status' => true,
                'msg' => 'Lấy số thứ tự thành công',
                'data' => $customer,
            ];
            return \response()->json($dataReturn, Response::HTTP_OK);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }

    public function callNumber (Request $request)
    {
        try{
            $agency = DonviModel::join('user', 'user.id', '=', 'donvi.user_id')->select('donvi.*')->where('user.token', $request->get('token'))->first();
            if (isset($agency)){
                $service = ServiceModel::where('id', $request->get('id_dichvu'))->where('id_donvi', $agency->id)->first();
                if (empty($service)){
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'Dịch vụ không tồn tại',
                    ];
                    return response()->json($dataReturn, Response::HTTP_OK);
                }
                $is_call = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->where('khach_isActive', 1)->get()->last();
                $call = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->orderBy('khach_giolayso', 'asc')->first();
                $time_wait = strtotime(Carbon::now('Asia/Ho_Chi_Minh')) - strtotime($call->khach_giolayso);
                $call->khach_isActive = 1;
                $call->khach_giogoiso = Carbon::now('Asia/Ho_Chi_Minh');
                $call->khach_thoigiancho = gmdate('H:i:s', $time_wait);
                $call->save();
                $service->dichvu_sodanggoi = $call->khach_sothutu;
                $service->save();
                if (isset($is_call)){
                    $time_end = strtotime(Carbon::now('Asia/Ho_Chi_Minh')) - strtotime($is_call->khach_giogoiso);
                    $is_call->khach_gioketthuc = Carbon::now('Asia/Ho_Chi_Minh');
                    $is_call->khach_thoigianphucvu = gmdate('H:i:s', $time_end);
                    $is_call->save();
                }
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Gọi số thành công',
                    'data' => $call
                ];
            }else{
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Đơn vị không tồn tại',
                ];
            }
            return response()->json($dataReturn, Response::HTTP_OK);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }

    public function getToken (Request $request)
    {
        $arr = [
            'username' => trim($request->get('phone')),
            'password' => trim($request->get('pass')),
            'user_phanquyen' => 2
        ];
        if(Auth::guard('agency')->attempt($arr)){
            $user = Auth::guard('agency')->user();
            $dataReturn = [
                'status' => true,
                'token' => $user['token']
            ];
            return $dataReturn;
        }else{
            $dataReturn = [
                'status' => false,
                'msg' => 'Số điện thoại hoặc mật khẩu không đúng'
            ];
        }
        return response()->json($dataReturn, 200);
    }

    public function dongBoDichVu (Request $request)
    {
        try{
            if (isset($request->data) && $request->data == 'dichvu'){
                $agency = DonviModel::find($request->get('id_donvi'));
                if (isset($agency)){
                    $listService = ServiceModel::where('id_donvi', $agency->id)->where('dichvu_isSync', 0)->get();
                    if (count($listService)){
                        $data['dichvu'] = $listService;
                    }else{
                        $data['id_donvi'] = $request->get('id_donvi');
                        $data['dichvu'] = null;
                    }
                }else{
                    $data = [
                        'status' => false,
                        'msg' => 'Đơn vị không tồn tại'
                    ];
                }
            }elseif (isset($request->bang) && $request->bang == 'dichvu'){
                $service = ServiceModel::where('id', $request->get('id_record'))->where('id_donvi', $request->get('id_donvi'))->first();
                if (empty($service)){
                    $data = [
                        'status' => false,
                        'msg' => 'Dịch vụ không tồn tại'
                    ];
                }else{
                    if ($request->dichvu_isSync == 'true'){
                        $service->dichvu_isSync = 1;
                        $service->save();
                        $data = [
                            'status' => true,
                            'msg' => 'Cập nhật dịch vụ thành công'
                        ];
                    }else{
                        $data = [
                            'status' => false,
                            'msg' => 'Cập nhật dịch vụ thất bại'
                        ];
                    }
                }
            }else{
                $data = [
                    'status' => false,
                    'msg' => 'Dữ liệu chuyền lên không chính xác'
                ];
            }
            return response()->json($data, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }

    public function dongBoDonVi (Request $request)
    {
        try{
            if (isset($request->data) && $request->data == 'donvi'){
                $agency = DonviModel::where('id', $request->get('id_donvi'))->where('donvi_isSync', 0)->get();
                if (count($agency)){
                    $dataReturn ['donvi'] = $agency;
                }else{
                    $dataReturn = [
                        'id_donvi' => $request->get('id_donvi'),
                        'donvi' => null
                    ];
                }
            }elseif (isset($request->bang) && $request->bang == 'donvi'){
                $agency = DonviModel::where('id', $request->get('id_donvi'))->first();
                if (empty($agency)){
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'Đơn vị không tồn tại'
                    ];
                }else{
                    if ( isset($request->donvi_isSync) && $request->get('donvi_isSync') == 'true'){
                        $agency->donvi_isSync = 1;
                        $agency->save();
                        $dataReturn = [
                            'status' => true,
                            'msg' => 'Đồng bộ đơn vị thành công'
                        ];
                    }else{
                        $dataReturn = [
                            'status' => false,
                            'msg' => 'Đồng bộ đơn vị thất bại'
                        ];
                    }
                }
            }else{
                $dataReturn = [
                    'status' => false,
                    'msg' => "Dữ liệu chuyền lên không chính xác"
                ];
            }
            return response()->json($dataReturn, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }catch (Exception $exception){
            return \response()->json($exception->getMessage(), Response::HTTP_FOUND);
        }
    }

    public function dongBoMayIn (Request $request)
    {
        try{
            if (isset($request->data) && $request->data == 'mayin'){
                $listData = PrinterModel::where('id_donvi', $request->get('id_donvi'))->where('mayin_isSync', 0)->get();
                if (count($listData)){
                    $dataReturn['mayin'] = $listData;
                }else{
                    $dataReturn = [
                        'id_donvi' => $request->get('id_donvi'),
                        'mayin' => null
                    ];
                }
            }elseif (isset($request->bang) && $request->bang == 'mayin'){
                $printer = PrinterModel::where('id_donvi', $request->get('id_donvi'))->where('id', $request->get('id_record'))->first();
                if (isset($printer)){
                    if ($request->mayin_isSync == 'true'){
                        $printer->mayin_isSync = 1;
                        $printer->save();
                        $dataReturn = [
                            'status' => true,
                            'msg' => 'Đồng bộ máy in thành công'
                        ];
                    }else{
                        $dataReturn = [
                            'status' => false,
                            'msg' => 'Đồng bộ máy in thất bại'
                        ];
                    }
                }else{
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'Máy in không tồn tại'
                    ];
                }
            }else{
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Dữ liệu chuyền lên không chính xác'
                ];
            }
            return response()->json($dataReturn, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }catch (\Exception $exception){
            return \response()->json($exception->getMessage(), Response::HTTP_FOUND);
        }
    }

    public function dongBoKhachHang (Request $request)
    {
        try{
            if (isset($request->data) && $request->data == 'khachhang'){
                if (isset($request->khach_ngay)){
                    $newDate = Carbon::createFromFormat('m/d/Y', $request->khach_ngay)->format('Y-m-d');
                    $data = CustomerModel::where('id_donvi', $request->get('id_donvi'))->where('khach_isSync', 0)
                        ->whereDate('khach_giolayso', $newDate)->orderBy('created_at', 'desc')->get();
                }else{
                    $data = CustomerModel::where('id_donvi', $request->get('id_donvi'))->where('khach_isSync', 0)
                        ->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->orderBy('created_at', 'desc')->get();
                }
                if (count($data)){
                    $dataReturn = [
                        'khachhang' => $data
                    ];
                }else{
                    $dataReturn = [
                        'id_donvi' => $request->get('id_donvi'),
                        'khachhang' => null
                    ];
                }
            }elseif (isset($request->bang) && $request->bang == 'khachhang'){
                $data = CustomerModel::where('id_donvi', $request->get('id_donvi'))->where('id', $request->get('id_record'))->first();
                if (isset($data)){
                    if ($request->khach_isSync == 'true'){
                        $data->khach_isSync = 1;
                        $data->save();
                        $dataReturn = [
                            'status' => true,
                            'msg' => 'Đồng bộ khách hàng thành công'
                        ];
                    }else{
                        $dataReturn = [
                            'status' => false,
                            'msg' => 'Đồng bộ khách hàng thất bại'
                        ];
                    }
                }else{
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'Khách hàng không tồn tại'
                    ];
                }
            }else{
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Dữ liệu chuyền lên không chính xác'
                ];
            }
            return response()->json($dataReturn, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }catch (\Exception $exception){
            return \response()->json($exception->getMessage(), Response::HTTP_FOUND);
        }
    }

    public function khachHangLaySoThuTu (Request $request)
    {
        try{
            $service = ServiceModel::where('id_donvi', $request->get('id_donvi'))->where('id_localservice', $request->get('id_localservice'))->first();
            $agency = DonviModel::find($request->get('id_donvi'));
            if (empty($service)){
                $data = [
                    'status' => false,
                    'msg' => 'Dịch vụ không tồn tại'
                ];
                return response()->json($data, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                    JSON_UNESCAPED_UNICODE);
            }
            $customer = new CustomerModel([
                'id_donvi' => $request->get('id_donvi'),
                'ten_donvi' => $agency->donvi_ten,
                'id_dichvu' => $service->id,
                'ten_dichvu' => $service->dichvu_ten,
                'id_mayin' => 0,
                'khach_sothutu' => $request->get('khach_sothutu'),
                'khach_madatso' => $request->get('khach_madatso'),
                'khach_giolayso' => Carbon::createFromFormat('m/d/Y H:i:s', $request->get('khach_giolayso'))->format('Y-m-d H:i:s'),
                'khach_giogoiso' => Carbon::createFromFormat('m/d/Y H:i:s', $request->get('khach_giogoiso'))->format('Y-m-d H:i:s'),
                'khach_gioketthuc' => Carbon::createFromFormat('m/d/Y H:i:s', $request->get('khach_gioketthuc'))->format('Y-m-d H:i:s'),
                'khach_thoigiancho' => $request->get('khach_thoigiancho'),
                'khach_thoigianphucvu' => $request->get('khach_thoigianphucvu'),
                'khach_tenkhach' => $request->get('khach_tenkhach'),
                'khach_dienthoai' => $request->get('khach_sodienthoai'),
                'khach_isSync' => 1,
            ]);
            $customer->save();
            $service->dichvu_sodanggoi = $request->get('khach_sothutu');
            $service->save();
            $data = [
                'bang' => 'khachhang',
                'id_donvi' => $agency->id,
                'id_khach' => $request->get('id_khach'),
                'khach_isSync' => true
            ];
            return \response()->json($data, Response::HTTP_OK);
        }catch (\Exception $exception){
            return \response()->json($exception->getMessage(), Response::HTTP_FOUND);
        }
    }
}
