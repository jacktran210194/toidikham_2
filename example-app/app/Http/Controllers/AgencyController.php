<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use App\Models\DonviModel;
use App\Models\ServiceModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use PHPUnit\Exception;

class AgencyController extends Controller
{
    public function doLogin (Request $request)
    {
        try{
            $arr = [
                'username' => trim($request->get('phone')),
                'password' => trim($request->get('password')),
                'user_phanquyen' => 2
            ];
            $user = User::where('username', $arr['username'])
                ->where('user_phanquyen', 2)
                ->value('id');
            if (empty($user)){
                return redirect()->back()->with('error', 'Tài khoản không tồn tại');
            }
            if(Auth::guard('agency')->attempt($arr)){
                return redirect()->route('agency.index');
            }else{
                return back()->withErrors(['error' => 'Tài khoản hoặc mật khẩu không đúng']);
            }
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Đăng xuất
    **/
    public function logOut ()
    {
        Auth::guard('agency')->logout();
        return redirect()->route('agency.login')->with(['success' => 'Đăng xuất thành công']);
    }
    /**
     * Tổng quan
    **/
    public function index (Request $request)
    {
        $titlePage = 'Đơn Vị - Tổng Quan';
        $namePage = 'dashboard';
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $service = ServiceModel::join('donvi', 'donvi.id', '=', 'dichvu.id_donvi')->select('dichvu.*')
            ->where('donvi.user_id', $this->getUserIDAgency())->orderBy('dichvu.dichvu_ten')->get();
        if ($service){
            foreach ($service as $value){
                $value->total = CustomerModel::where('id_donvi', $agency->id)->where('id_dichvu', $value->id)->count();
            }
        }
        // Lấy danh sách người dùng đặt stt //
        $listUser = CustomerModel::query();
        if (isset($request->date_form) && isset($request->date_to)){
            $listUser = $listUser->whereDate('khach_giolayso', '>=', $request->get('date_form'))
                ->whereDate('khach_giolayso', '<=', $request->get('date_to'));
        }
        if (isset($request->service)){
            $listUser = $listUser->where('id_dichvu', $request->get('service'));
        }
        $listUser = $listUser->where('id_donvi', $agency->id)->orderBy('created_at', 'desc')->get();
        return view('agency.dashboard', compact('titlePage', 'namePage', 'service', 'listUser'));
    }
    /**
     * Tạo mới dich vụ
    **/
    public function createService (Request $request)
    {
        try{
            $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
            ServiceModel::createService($agency->id,$request->get('dichvu_ten'),$request->get('dichvu_sobatdau'),
                $request->get('dichvu_socuoi'), $request->get('dichvu_sotoida'),
                $request->get('bg_color'), $request->get('id_local'), $request->get('dichvu_ghichu'));
            return back()->with(['success' => 'Tạo mới dịch vụ thành công']);
        }catch (Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Danh sách dịch vụ
    **/
    public function service ()
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $service = ServiceModel::where('id_donvi', $agency->id)->orderBy('created_at', 'desc')->get();
        $titlePage = 'Đơn vị - Dịch vụ';
        $namePage = 'service';
        $subName = 'index';
        return view('agency.service_index', compact('agency', 'service', 'titlePage', 'namePage', 'subName'));
    }
    /**
     * Thông Tin Dịch Vụ
    **/
    public function modalService (Request $request)
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $service = ServiceModel::where('id_donvi', $agency->id)->where('id', $request->get('service_id'))->first();
        if (empty($service)){
            $data['status'] = false;
            $data['msg'] = 'Dịch vụ không tồn tại';
            return $data;
        }
        $view = view('agency.popup_service', compact('service'))->render();
        return response()->json(['status' => true, 'html' => $view]);
    }
    /**
     * Chi tiết dịch vụ
    **/
    public function detailsService (Request $request, $id)
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $service = ServiceModel::where('id_donvi', $agency->id)->where('id',$id)->first();
        if (empty($service)){
            return back()->with(['error' => 'Dịch vụ không tồn tại']);
        }
        // Lấy danh sách người dùng đặt stt //
        $listUser = CustomerModel::query();
        if (isset($request->date_form) && isset($request->date_to)){
            $listUser = $listUser->whereDate('khach_giolayso', '>=', $request->get('date_form'))
                ->whereDate('khach_giolayso', '<=', $request->get('date_to'));
        }
        $listUser = $listUser->where('id_dichvu', $id)->orderBy('created_at', 'desc')->get();
        $titlePage = 'Đơn Vị - Dịch Vụ';
        $namePage = 'dashboard';
        return view('agency.service_details', compact('service', 'listUser', 'titlePage', 'namePage'));
    }

    /**
     * Cập nhật dịch vụ
    **/
    public function updateService (Request $request, $id)
    {
        try{
            $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
            $service = ServiceModel::where('id_donvi', $agency->id)->where('id',$id)->first();
            if (empty($service)){
                return back()->with(['error' => 'Dịch vụ không tồn tại']);
            }
            $active = isset($request->active) ? 1 : 0;
            ServiceModel::updateService($service,$request->get('dichvu_ten'), $request->get('dichvu_sobatdau'),
                $request->get('dichvu_socuoi'), $request->get('dichvu_sotoida'),$request->get('bg_color'),
                $active,$request->get('id_local'), $request->get('dichvu_ghichu'));
            return back()->with(['success' => 'update success']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Xóa bỏ dịch vụ
    **/
    public function deleteService ($id)
    {
        try{
            $service = ServiceModel::join('donvi', 'donvi.id', '=', 'dichvu.id_donvi')->select('dichvu.*')
                ->where('donvi.user_id', $this->getUserIDAgency())->where('dichvu.id', $id)->first();
            if (empty($service)){
                return back()->with(['error' => 'Dịch vụ không tồn tại']);
            }
            $service->delete();
            return back();
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Xuất excel của dịch vụ
    **/
    public function excel (Request $request, $id)
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $service = ServiceModel::where('id_donvi', $agency->id)->where('id',$id)->first();
        if (empty($service)){
            return back()->with(['error' => 'Dịch vụ không tồn tại']);
        }
        // Lấy danh sách người dùng đặt stt //
        $listUser = CustomerModel::query();
        if (isset($request->date_form) && isset($request->date_to)){
            $listUser = $listUser->whereDate('khach_giolayso', '>=', $request->get('date_form'))
                ->whereDate('khach_giolayso', '<=', $request->get('date_to'));
        }
        $listUser = $listUser->where('id_dichvu', $id)->orderBy('created_at', 'desc')->get();
        $file_name = Str::slug($service->dichvu_ten);
        $this->exportExcel($listUser,$file_name,$service->dichvu_ten);
        return back();
    }
    /**
     * Lấy dữ liệu để làm biểu đồ khách hàng đặt stt theo tháng
    **/
    public function getDataAreaChart (Request $request)
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $data_number = array();
        if (isset($request->date_form) && isset($request->date_to)){
            $dataDate['dates'] = [];
            $start_date = strtotime($request->date_form);
            $end_date = strtotime($request->date_to);
            while ($start_date <= $end_date) {
                $number = CustomerModel::where('id_donvi', $agency->id)->whereDate('khach_giolayso', date("Y-m-d", $start_date))->count();
                array_push($data_number, $number);
                array_push($dataDate['dates'], date("d-m-Y", $start_date));
                $start_date = strtotime("+1 day", $start_date);
            }
        }else{
            $dataDate = $this->getDay();
            foreach ($dataDate['dataDate'] as $value){
                $number = CustomerModel::where('id_donvi', $agency->id)->whereDate('khach_giolayso', $value)->count();
                array_push($data_number, $number);
            }
        }
        $dataReturn = [
            'date' => $dataDate['dates'],
            'number' => $data_number
        ];
        return response()->json($dataReturn,200);
    }

    public function getDataBarChart ()
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        $month = date('m');$year = date('Y');
        $dataMonth = [];$dataNumber = [];
        for ($i = 1; $i <= $month; $i++){
            array_push($dataMonth, $i);
        }
        foreach ($dataMonth as $value){
            $number = CustomerModel::where('id_donvi', $agency->id)->whereMonth('khach_giolayso', $value)->whereYear('khach_giolayso', $year)->count();
            array_push($dataNumber, $number);
        }
        $dataReturn = [
            'month' => $dataMonth,
            'number' => $dataNumber
        ];
        return response()->json($dataReturn,200);
    }

    /**
     * Xuất excel của đơn vị
    **/
    public function excelIndex (Request $request)
    {
        $agency = DonviModel::where('user_id', $this->getUserIDAgency())->first();
        // Lấy danh sách người dùng đặt stt //
        $listUser = CustomerModel::query();
        if (isset($request->date_form) && isset($request->date_to)){
            $listUser = $listUser->whereDate('khach_giolayso', '>=', $request->get('date_form'))
                ->whereDate('khach_giolayso', '<=', $request->get('date_to'));
        }
        $listUser = $listUser->where('id_donvi', $agency->id)->orderBy('created_at', 'desc')->get();
        $file_name = Str::slug($agency->donvi_ten);
        $this->exportExcel($listUser,$file_name,$agency->donvi_ten);
        return back();
    }
    /**
     * Màn gọi số thứ tự
    **/
    public function callNumber ()
    {
        $titlePage = 'Đơn Vị - Gọi Số Thứ Tự';
        $namePage = 'call_number';
        $agency = DonviModel::join('user', 'user.id', '=', 'donvi.user_id')->select('donvi.*', 'user.token')->where('user.id', $this->getUserIDAgency())->first();
        $listService = ServiceModel::where('id_donvi', $agency->id)->get();
        $service = ServiceModel::where('id_donvi', $agency->id)->first();
        if (empty($service)){
            return back()->with(['error' => 'Vui lòng thêm dịch vụ']);
        }
        $listCustomer = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->where('khach_isActive', 0)
            ->orderBy('khach_giolayso', 'asc')->limit(3)->get();
        $listAllCustomer = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->orderBy('khach_isActive', 'asc')->orderBy('khach_giolayso', 'asc')->get();
        $is_call = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->where('khach_isActive', 1)->get()->last();
        return view('agency.call_number', compact('titlePage', 'namePage','listService','listCustomer','listAllCustomer', 'service', 'is_call','agency'));
    }

    /**
     * Màn gọi số thứ tự theo dich vu
    **/
    public function callNumberService ($id)
    {
        $titlePage = 'Đơn Vị - Gọi Số Thứ Tự';
        $namePage = 'call_number';
        $agency = DonviModel::join('user', 'user.id', '=', 'donvi.user_id')->select('donvi.*', 'user.token')->where('user.id', $this->getUserIDAgency())->first();
        $listService = ServiceModel::where('id_donvi', $agency->id)->get();
        $service = ServiceModel::where('id_donvi', $agency->id)->where('id', $id)->first();
        if (empty($service)){
            return back()->with(['error' => 'Dịch vụ không tồn tại']);
        }
        $listCustomer = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->where('khach_isActive', 0)
            ->orderBy('khach_giolayso', 'asc')->limit(3)->get();
        $listAllCustomer = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->orderBy('khach_giolayso', 'asc')->get();
        $is_call = CustomerModel::where('id_dichvu', $service->id)->whereDate('khach_giolayso', Carbon::now('Asia/Ho_Chi_Minh'))->where('khach_isActive', 1)->get()->last();
        return view('agency.call_number', compact('titlePage', 'namePage','listService','listCustomer','listAllCustomer', 'service', 'is_call','agency'));
    }
}
