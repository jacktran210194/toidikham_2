<?php

namespace App\Http\Controllers;

use App\Models\DonviModel;
use App\Models\PrinterModel;
use App\Models\SectorModel;
use App\Models\ServiceModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Exception;

class AdminController extends Controller
{
    /**
     * Đăng nhập
    **/
    public function doLogin (Request $request)
    {
        try{
            $arr = [
                'username' => trim($request->get('phone')),
                'password' => trim($request->get('password')),
                'user_phanquyen' => 3
            ];
            $user = User::where('username', $arr['username'])
                ->where('user_phanquyen', 3)
                ->value('id');
            if (empty($user)){
                return redirect()->back()->with('error', 'Tài khoản không tồn tại');
            }
            if(Auth::guard('admin')->attempt($arr)){
                return redirect()->route('admin.index');
            }else{
                return back()->with(['error' => 'Tài khoản hoặc mật khẩu không đúng']);
            }
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Đăng xuất
    **/
    public function logout ()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
    /**
     * Quản lý đơn vị
    **/
    public function index (Request $request)
    {
        if (isset($request->name)){
            $listData = DonviModel::where('donvi_ten', 'Like', '%'.$request->get('name').'%')->orderBy('donvi_ten')->paginate(20);
        }else{
            $listData = DonviModel::orderBy('donvi_ten')->paginate(20);
        }
        foreach ($listData as $value){
            $value->sector = SectorModel::find($value->id_linhvuc);
        }
        $sector = SectorModel::all();
        $titlePage = 'Admin';
        $namePage = 'agency';
        return view('admin.index', compact('listData', 'titlePage', 'namePage','sector'));
    }
    /**
     * Xóa đơn vị
    **/
    public function deleteAgency ($id)
    {
        $agency = DonviModel::find($id);
        if (empty($agency)){
            return back()->with(['error' => 'Đơn vị không tồn tại']);
        }
        $user = User::find($agency->user_id);
        $user->delete();
        ServiceModel::where('id_donvi', $id)->delete();
        $agency->delete();
        return back()->with(['success' => 'delete success']);
    }
    /**
     * Show thông tin đơn vị
    **/
    public function showAgency (Request $request)
    {
        try{
            $agency = DonviModel::find($request->get('id'));
            if (empty($agency)){
                return response()->json(['status' => false, 'msg' => 'Đơn vị không tồn tại']);
            }
            $sector = SectorModel::all();
            $view = view('admin.popup_agency', compact('agency', 'sector'))->render();
            return response()->json(['status' => true, 'html' => $view]);
        }catch (Exception $exception){
            return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
        }
    }
    /**
     * Cập nhật đơn vị
    **/
    public function updateAgency (Request $request, $id)
    {
        try{
            $agency = DonviModel::find($id);
            if (empty($agency)){
                return back()->with(['error' => 'Đơn vị không tồn tại']);
            }
            $active = 0;
            if (isset($request->active)){
                $active = 1;
            }
            DonviModel::updateAgency($agency,$request->get('donvi_ten'),$request->get('donvi_diachi'),$request->get('id_linhvuc'),$request->get('donvi_ghichu'),$active);
            return back()->with(['success' => 'Cập nhật đơn vị thành công']);
        }catch (\Exception $exception){
            return back()->with(['error'=> $exception->getMessage()]);
        }
    }
    /**
     * Tạo mới đơn vị
    **/
    public function storeAgency (Request $request)
    {
        try{
            $user = User::where('username', $request->get('phone'))->first();
            $active = 0;
            if (isset($request->active)){
                $active = 1;
            }
            if (isset($user)){
                if ($user->user_phanquyen == 3 || $user->user_phanquyen == 2){
                    return back()->with(['error' => 'Số điện thoại đã đươc đăng ký']);
                }else{
                    DonviModel::createAgency($user->id, $request->get('donvi_ten'),$request->get('id_linhvuc'), $request->get('donvi_diachi'),$request->get('donvi_ghichu'),$active);
                    $user->user_phanquyen = 2;
                    $user->save();
                    return back()->with(['success' => 'Tạo mới đơn vị thành công']);
                }
            }else{
                $user = User::createUser($request->get('donvi_ten'),$request->get('phone'),$request->get('password'),2);
                DonviModel::createAgency($user->id, $request->get('donvi_ten'),$request->get('id_linhvuc'), $request->get('donvi_diachi'),$request->get('donvi_ghichu'),$active);
                return back()->with(['success' => 'Tạo mới đơn vị thành công']);
            }
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Danh sách người dùng
    **/
    public function user (Request $request)
    {
        $listData = User::query();
        if (isset($request->name)){
            $listData = $listData->where('user_hoten', 'Like', '%'.$request->get('name').'%')
                ->orWhere('username', 'Like', '%'.$request->get('name').'%');
        }
        $listData = $listData->whereIn('user_phanquyen', [1,2])->orderBy('user_hoten')->paginate(40);
        $titlePage = 'Admin';
        $namePage = 'user';
        return view('admin.user', compact('listData', 'titlePage', 'namePage'));
    }
    /**
     * Xóa người dùng
    **/
    public function deleteUser ($id)
    {
        $user = User::find($id);
        if (empty($user)){
            return back()->with(['error' => 'Người dùng không tồn tại']);
        }
        if ($user->user_phanquyen == 2){
            $agency = DonviModel::where('user_id', $id)->first();
            ServiceModel::where('id_donvi', $agency->id)->delete();
            $agency->delete();
        }
        $user->delete();
        return back()->with(['success' => 'Xóa thành công']);
    }
    /**
     * Show người dùng
    **/
    public function showUser (Request $request)
    {
        try{
            $user = User::find($request->get('id'));
            if (empty($user)){
                return response()->json(['status' => false, 'msg' => 'Người dùng không tồn tại']);
            }
            $view = view('admin.popup_user', compact('user'))->render();
            return response()->json(['status' => true, 'html' => $view]);
        }catch (Exception $exception){
            return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
        }
    }
    /**
     * Cập nhật người dùng
    **/
    public function updateUser (Request $request, $id)
    {
        try{
            $user = User::find($id);
            if (empty($user)){
                return back()->with(['error' => 'Người dùng không tồn tại']);
            }
            $user->user_hoten = $request->get('user_hoten');
            $user->user_email = $request->get('user_email');
            $user->user_ghichu = $request->get('user_ghichu');
            if (isset($request->password)){
                $user->password = bcrypt($request->get('password'));
            }
            $user->save();
            return back()->with(['success' => 'Cập nhật thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Tạo mới người dùng
    **/
    public function storeUser (Request $request)
    {
        try{
            $user = User::where('username', $request->get('username'))->first();
            if ($user){
                return back()->with(['error' => 'Số điện thoại của người dùng đã được đăng ký']);
            }
            User::createUser($request->get('user_hoten'),$request->get('username'),$request->get('password'), 1,$request->get('user_email'),$request->get('user_ghichu'));
            return back()->with(['success' => 'Tạo mới thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Danh sách lĩnh vực
    **/
    public function listSector (Request $request)
    {
        $titlePage = 'Admin';
        $namePage = 'sector';
        if (isset($request->name)){
            $listData = SectorModel::where('linhvuc_ten', 'Like', '%'.$request->get('name').'%')->orderBy('linhvuc_ten')->get();
        }else{
            $listData = SectorModel::orderBy('linhvuc_ten')->get();
        }
        return view('admin.sector', compact('titlePage', 'namePage', 'listData'));
    }

    /**
     * Xóa lĩnh vực
    **/
    public function deleteSector ($id)
    {
        $sector = SectorModel::find($id);
        if (empty($sector)){
            return back()->with(['error' => 'Không tìm thấy lĩnh vực']);
        }
        $sector->delete();
        return back()->with(['success' => 'Xóa lĩnh vực thành công']);
    }

    /**
     * Show lĩnh vực
     **/
    public function showSector (Request $request)
    {
        try{
            $sector = SectorModel::find($request->get('id'));
            if (empty($sector)){
                return response()->json(['status' => false, 'msg' => 'Lĩnh vực không tồn tại']);
            }
            $view = view('admin.popup_sector', compact('sector'))->render();
            return response()->json(['status' => true, 'html' => $view]);
        }catch (Exception $exception){
            return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
        }
    }
    /**
     * Cập nhật lĩnh vực
    **/
    public function updateSector (Request $request, $id)
    {
        try{
            $sector = SectorModel::find($id);
            if (empty($sector)){
                return back()->with(['error' => 'Lĩnh vực không tồn tại']);
            }
            $sector->linhvuc_ten = $request->get('linhvuc_ten');
            $sector->linhvuc_ghichu = $request->get('linhvuc_ghichu');
            $sector->save();
            return back()->with(['success' => 'Cập nhật lĩnh vực thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Tạo mới lĩnh vực
    **/
    public function storeSector (Request $request)
    {
        try{
            $sector = SectorModel::where('linhvuc_ten', $request->get('linhvuc_ten'))->first();
            if (isset($sector)){
                return back()->with(['error' => 'Lĩnh vực đã tồn tại']);
            }
            SectorModel::createSector($request->get('linhvuc_ten'), $request->get('linhvuc_ghichu'));
            return back()->with(['success' => 'Tạo mới thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Danh sách dịch vụ
    **/
    public function service (Request $request)
    {
        $titlePage = 'Admin';
        $namePage = 'service';
        $listData = ServiceModel::query();
        $listData = $listData->join('donvi', 'donvi.id', '=', 'dichvu.id_donvi')->select('dichvu.*', 'donvi.donvi_ten');
        if (isset($request->name)){
            $listData = $listData->where('dichvu.dichvu_ten', 'Like', '%'.$request->get('name').'%');
        }
        if (isset($request->id_donvi)){
            $listData = $listData->where('dichvu.id_donvi', $request->get('id_donvi'));
        }
        $listData = $listData->orderBy('dichvu.dichvu_ten')->paginate(20);
        $listAgency = DonviModel::orderBy('donvi_ten')->get();
        return view('admin.service', compact('titlePage', 'namePage', 'listData','listAgency'));
    }

    public function deleteService ($id)
    {
        $service = ServiceModel::find($id);
        if (empty($service)){
            return back()->with(['error' => 'Dịch vụ không tồn tại']);
        }
        $service->delete();
        return back()->with(['success' => 'Xóa thành công']);
    }

    public function showService (Request $request)
    {
        $service = ServiceModel::find($request->get('id'));
        if (empty($service)){
            $data['status'] = false;
            $data['msg'] = 'Dịch vụ không tồn tại';
            return $data;
        }
        $view = view('admin.popup_service', compact('service'))->render();
        return response()->json(['status' => true, 'html' => $view]);
    }

    public function updateService(Request $request, $id)
    {
        try{
            $service = ServiceModel::find($id);
            if (empty($service)){
                return back()->with(['error' => 'Dịch vụ không tồn tại']);
            }
            $active = isset($request->active) ? 1 : 0;
            ServiceModel::updateService($service,$request->get('dichvu_ten'), $request->get('dichvu_sobatdau'),
                $request->get('dichvu_socuoi'), $request->get('dichvu_sotoida'),$request->get('bg_color'),
                $active,$request->get('dichvu_ghichu'));
            return back()->with(['success' => 'Cập nhật thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    public function createService (Request $request)
    {
        try{
            $agency = DonviModel::find($request->get('id_donvi'));
            if (empty($agency)){
                return back()->with(['error' => 'Không tìm thấy đơn vị']);
            }
            ServiceModel::createService($agency->id,$request->get('dichvu_ten'),$request->get('dichvu_sobatdau'),
                $request->get('dichvu_socuoi'), $request->get('dichvu_sotoida'), $request->get('bg_color'), $request->get('dichvu_ghichu'));
            return back()->with(['success' => 'Tạo mới dịch vụ thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Máy in
    **/
    public function printer (Request $request)
    {
        $listData = PrinterModel::query();
        $listData = $listData->join('donvi', 'donvi.id', '=', 'mayin.id_donvi')->select('mayin.*', 'donvi.donvi_ten');
        if (isset($request->name)){
            $listData = $listData->where('mayin.mayin_tenmay', 'Like', '%'.$request->get('name').'%');
        }
        if (isset($request->id_donvi)){
            $listData = $listData->where('donvi.id', $request->get('id_donvi'));
        }
        $listData = $listData->orderBy('mayin_tenmay')->paginate(20);
        $listAgency = DonviModel::orderBy('donvi_ten')->get();
        $titlePage = 'Admin';
        $namePage = 'printer';
        return view('admin.printer', compact('listData', 'listAgency','titlePage', 'namePage'));
    }

    public function deletePrint ($id)
    {
        $printer = PrinterModel::find($id);
        if (empty($printer)){
            return back()->with(['error' => 'Máy in không tồn tại']);
        }
        $printer->delete();
        return back()->with(['success' => 'Xóa thành công']);
    }

    public function showPrint (Request $request)
    {
        $printer = PrinterModel::find($request->get('id'));
        if (empty($printer)){
            $data['status'] = false;
            $data['msg'] = 'Không tìm thấy dữ liệu';
            return $data;
        }
        $view = view('admin.printer_details', compact('printer'))->render();
        return response()->json(['status' => true, 'html' => $view]);
    }

    public function updatePrinter (Request $request, $id)
    {
        try{
            $printer = PrinterModel::find($id);
            if (empty($printer)){
                return back()->with(['error' => 'Không tìm thấy máy in']);
            }
            PrinterModel::updatePrinter($printer,$request->name,$request->uid_code,$request->address,$request->line1,$request->line2,1,$request->soline,
                1,$request->startMorning,$request->endMorrning,$request->startAfternoon,$request->endAfternoon);
            return back()->with(['success' => 'Cập nhật thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }

    public function createPrinter (Request $request)
    {
        try{
            $agency = DonviModel::find($request->get('id_donvi'));
            if (empty($agency)){
                return back()->with(['error' => 'Đơn vị không tồn tại']);
            }
            PrinterModel::createPrinter($agency->id,$request->name, $request->uid_code,$request->address,$request->line1,$request->line2,1,
                $request->soline,1,$request->startMorning,$request->endMorrning,$request->startAfternoon,$request->endAfternoon);
            return back()->with(['success' => 'Tạo mới máy in thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
}
