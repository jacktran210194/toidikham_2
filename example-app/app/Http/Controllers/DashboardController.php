<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use App\Models\DonviModel;
use App\Models\SectorModel;
use App\Models\ServiceModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    public function index ()
    {
        $sector = SectorModel::orderBy('linhvuc_ten')->get();
        $agency = DonviModel::where('donvi_isActive', 1)->orderBy('donvi_ten')->get();
        return view('welcome', compact('sector', 'agency'));
    }
    /**
     * Lấy danh sách đơn vị
    **/
    public function getAgency (Request $request)
    {
        try{
            $agency = DonviModel::where('id_linhvuc', $request->get('value'))->where('donvi_isActive', 1)->orderBy('donvi_ten')->get();
            $html = '<option value="">Chọn Đơn Vị</option>';
            foreach ($agency as $value){
                $html .= '<option value="'.$value->id.'">'.$value->donvi_ten.'</option>';
            }
            $data = [
                'status' => true,
                'html' => $html
            ];
            return response()->json($data, Response::HTTP_OK);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }
    /**
     * Lấy danh sách dịch vụ
    **/
    public function getService (Request $request)
    {
        try{
            $service = ServiceModel::where('id_donvi', $request->get('value'))->where('dichvu_isActive', 1)->orderBy('dichvu_ten')->get();
            $html = '<option value="">Chọn Dịch Vụ</option>';
            foreach ($service as $value){
                $html .= '<option value="'.$value->id.'">'.$value->dichvu_ten.'</option>';
            }
            $data = [
                'status' => true,
                'html' => $html
            ];
            return response()->json($data, Response::HTTP_OK);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }

    /**
     * Tạo và lấy số thứ tự
    **/
    public function order (Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'agency' => 'required',
                'phone' => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:10|max:10',
                'service' => 'required',
            ], [
                'agency.required' => 'Vui lòng chọn đơn vị',
                'phone.required' => 'Vui lòng điền số điện thoại người dùng',
                'phone.regex' => 'Số điện thoại không đúng',
                'service.required' => 'Vui lòng chọn dịch vụ',
            ]);
            if ($validator->fails()) {
                $data['status'] = false;
                $data['msg'] = $validator->errors()->first();
                return $data;
            }
            $agency = DonviModel::find($request->get('agency'));
            $service = ServiceModel::where('id_donvi', $request->get('agency'))->where('id', $request->get('service'))->first();
            if ( empty($agency) || empty($service) || empty($request->phone)){
                $data['status'] = false;
                $data['msg'] = 'Dữ liệu chuyền không chính xác';
                return $data;
            }
            if (!$this->checkUserGetNumber($request->get('phone'), $service->id)){
                $data['status'] = false;
                $data['msg'] = 'Số điện thoại này đã được lấy stt trước đó. Vui lòng kiểm tra lại';
                return $data;
            }
            $number = $this->checkNumberLastOfService($service);
            if (!$number['status']){
                return $number;
            }
            $stt = $service->dichvu_sobatdau+$number['number']; // lấy số thứ tự
            $this->createUser($request->get('phone'), $request->get('name')); // Kiểm tra và tạo user
            $customer = CustomerModel::createCustomer(Auth::guard('user')->id(), $agency, $service, $stt, $request->get('name'), $request->get('phone')); //Tạo khách hàng
            $text_qrcode = $this->render_text_qrcode($customer);
            $view = view('order', compact('customer', 'text_qrcode', 'agency', 'service'))->render();
            return \response()->json(['status' => true, 'html' => $view, 'file_name' =>$customer->khach_madatso]);
        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    /**
     * Tra cứu số thứ tự
    **/
    public function filter (Request $request)
    {
        $customer = null;$service = null;
        if (count($request->all())){
            $customer = CustomerModel::query();
            if (isset($request->stt_code)){
                $customer = $customer->where('khach_madatso', $request->get('stt_code'))->orWhere('khach_dienthoai', $request->get('stt_code'));
            }
            if (isset($request->agency)){
                $customer = $customer->where('id_donvi', $request->get('agency'));
                $service = ServiceModel::where('id_donvi', $request->get('agency'))->where('dichvu_isActive', 1)->get();
            }
            if (isset($request->service)){
                $customer = $customer->where('id_dichvu', $request->get('service'));
            }
            $customer = $customer->whereDate('created_at', Carbon::now('Asia/Ho_Chi_Minh'))->orderBy('id', 'desc')->get();
        }
        if ($customer){
            foreach ($customer as $value){
                $value->text_qr = $this->render_text_qrcode($value);
                $value->agency = DonviModel::find($value->id_donvi);
                $value->service = ServiceModel::find($value->id_dichvu);
            }
        }
        $agency = DonviModel::where('donvi_isActive', 1)->get();
        return view('filter', compact('customer','service', 'agency'));
    }

    public function doLogin (Request $request)
    {
        $arr = [
            'username' => trim($request->get('phone')),
            'password' => trim($request->get('password')),
        ];
        if(Auth::guard('user')->attempt($arr)){
            return redirect()->intended('/');
        }else{
            return back()->withErrors(['message' => 'Tài khoản hoặc mật khẩu không đúng']);
        }
    }

    /**
     * Tạo tài khoản người dùng
    **/
    public function register (Request $request)
    {
        try{
            $user = User::where('username', $request->get('phone'))->first();
            if (isset($user)){
                return back()->withErrors(['message' => 'Số điện thoại đã tồn tại']);
            }
            User::createUser($request->get('name'),$request->get('phone'),$request->get('password'),1);
            return redirect()->route('web.login')->with(['success' => 'Đăng ký thành công']);
        }catch (\Exception $exception){
            return back()->withErrors(['message', $exception->getMessage()]);
        }
    }
    /**
     * Tao người dùng khi người dùng lấy stt
     **/
    public function createUser ($phone, $name)
    {
        $check_user = User::where('username', $phone)->first();
        if (empty($check_user)){
            if (empty($name)){
                $name = $phone;
            }
            User::createUser($name, $phone, 1234);
        }
        return true;
    }
}
