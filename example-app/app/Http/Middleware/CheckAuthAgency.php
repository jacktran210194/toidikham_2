<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAuthAgency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::guard('agency')->check()) {
            return redirect()->route('agency.login')->with(['error'=> 'Vui lòng đăng nhập để tiếp tục']);
        }
        if (Auth::guard('agency')->user()['user_phanquyen'] != 2){
            return redirect()->route('agency.login')->with(['error'=> 'Bạn không có quyền truy cập trang này']);
        }
        return $next($request);
    }
}
