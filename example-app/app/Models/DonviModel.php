<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonviModel extends Model
{
    use HasFactory;
    protected $table = 'donvi';
    protected $fillable = [
        'user_id',
        'id_linhvuc',
        'donvi_ten',
        'donvi_diachi',
        'donvi_ghichu',
        'donvi_isActive',
        'donvi_isSync'
    ];
    /**
     * Tạo tài khoản đơn vị
    **/
    public static function createAgency ($user_id, $name, $id_linhvuc, $address, $note, $active)
    {
        $agency = new DonviModel([
            'user_id' => $user_id,
            'donvi_ten' => $name,
            'donvi_isActive' => $active,
            'id_linhvuc' => $id_linhvuc,
            'donvi_ghichu' => $note,
            'donvi_diachi' => $address,
            'donvi_isSync' => 0
        ]);
        $agency->save();
        return $agency;
    }

    /**
     * Cập nhật tài khoản đơn vị
    **/
    public static function updateAgency ($agency, $name, $address, $id_linhvuc, $donvi_ghichu, $active)
    {
        $agency->donvi_ten = $name;
        $agency->donvi_diachi = $address;
        $agency->id_linhvuc = $id_linhvuc;
        $agency->donvi_ghichu = $donvi_ghichu;
        $agency->donvi_isActive = $active;
        $agency->donvi_isSync = 0;
        $agency->save();
        return $agency;
    }
}
