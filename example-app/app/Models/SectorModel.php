<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SectorModel extends Model
{
    use HasFactory;
    protected $table = 'linhvuc';
    protected $fillable = [
        'linhvuc_ten',
        'linhvuc_ghichu',
        'linhvuc_giotao'
    ];

    /**
     * Tạo Lĩnh Vực
    **/
    public static function createSector ($name, $note = null)
    {
        $sector = new SectorModel([
            'linhvuc_ten' => $name,
            'linhvuc_ghichu' => $note,
            'linhvuc_ghichu2' => Carbon::now('Asia/Ho_Chi_Minh')
        ]);
        $sector->save();
        return $sector;
    }
}
