<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrinterModel extends Model
{
    use HasFactory;
    protected $table = 'mayin';
    protected $fillable = [
        'id_donvi',
        'mayin_tenmay',
        'id_mayin',
        'mayin_uid',
        'mayin_diachi',
        'mayin_line1',
        'mayin_line2',
        'mayin_isQR',
        'mayin_solien',
        'mayin_isActive',
        'mayin_startMorning',
        'mayin_endMorrning',
        'mayin_startAfternoon',
        'mayin_endAfternoon',
        'mayin_isSync'
    ];
    /**
     * Danh sách máy in theo từng đơn vị
    **/
    public static function listPrinter ($id_agency)
    {
        $listPrinter = PrinterModel::where('id_donvi', $id_agency)->orderBy('mayin_tenmay')->get();
        return $listPrinter;
    }
    /**
     * Tạo mới máy in
    **/
    public static function createPrinter ($id_agency, $name, $uid, $address, $line1, $line2, $is_qr, $solien, $active, $startMorning, $endMorrning, $startAfternoon, $endAfternoon)
    {
        $printer = new PrinterModel([
            'id_donvi' => $id_agency,
            'mayin_tenmay' => $name,
            'mayin_uid' => $uid,
            'mayin_diachi' => $address,
            'mayin_line1' => $line1,
            'mayin_line2' => $line2,
            'mayin_isQR' => $is_qr,
            'mayin_solien' => $solien,
            'mayin_isActive' => $active,
            'mayin_startMorning' => $startMorning,
            'mayin_endMorrning' => $endMorrning,
            'mayin_startAfternoon' => $startAfternoon,
            'mayin_endAfternoon' => $endAfternoon,
            'mayin_isSync' => 0
        ]);
        $printer->save();
        return $printer;
    }
    /**
     * Cập nhật máy in
    **/
    public static function updatePrinter ($printer, $name, $uid, $address, $line1, $line2, $is_qr, $solien, $active, $startMorning, $endMorrning, $startAfternoon, $endAfternoon)
    {
        $printer->mayin_tenmay = $name;
        $printer->mayin_uid = $uid;
        $printer->mayin_diachi = $address;
        $printer->mayin_line1 = $line1;
        $printer->mayin_line2 = $line2;
        $printer->mayin_isQR = $is_qr;
        $printer->mayin_solien = $solien;
        $printer->mayin_isActive = $active;
        $printer->mayin_startMorning = $startMorning;
        $printer->mayin_endMorrning = $endMorrning;
        $printer->mayin_startAfternoon = $startAfternoon;
        $printer->mayin_endAfternoon = $endAfternoon;
        $printer->mayin_isSync = 0;
        $printer->save();
        return $printer;
    }
}
