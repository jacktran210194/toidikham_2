<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    use HasFactory;
    protected $table = 'khachhang';
    protected $fillable = [
        'user_id',
        'id_donvi',
        'ten_donvi',
        'id_dichvu',
        'ten_dichvu',
        'id_mayin',
        'khach_sothutu',
        'khach_madatso',
        'khach_giolayso',
        'khach_giogoiso',
        'khach_gioketthuc',
        'khach_thoigiancho',
        'khach_thoiganphucvu',
        'khach_tenkhach',
        'khach_dienthoai',
        'khach_isSync',
        'khach_isActive'
    ];

    public static function createCustomer ($user_id, $agency, $service, $stt, $name_customer, $phone_customer)
    {
        $customer = new CustomerModel([
            'user_id' => $user_id,
            'id_donvi' => $agency->id,
            'ten_donvi' => $agency->donvi_ten,
            'id_dichvu' => $service->id,
            'ten_dichvu' => $service->dichvu_ten,
            'id_mayin' => 0,
            'khach_sothutu' => $stt,
            'khach_madatso' => date('m').date('d').$stt.rand(111, 999),
            'khach_giolayso' => Carbon::now('Asia/Ho_Chi_Minh'),
            'khach_tenkhach' => $name_customer,
            'khach_dienthoai' => $phone_customer,
            'khach_isSync' => 0
        ]);
        $customer->save();
        return $customer;
    }
}
