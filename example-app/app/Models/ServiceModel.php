<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceModel extends Model
{
    use HasFactory;
    protected $table = 'dichvu';
    protected $fillable = [
        'id_donvi',
        'dichvu_ten',
        'dichvu_sobatdau',
        'dichvu_socuoi',
        'dichvu_sodanggoi',
        'dichvu_sotoida',
        'dichvu_isActive',
        'dichvu_ghichu',
        'id_localservice',
        'bg_color',
        'dichvu_isSync'
    ];

    /**
     * Tạo Dịch Vụ
    **/
    public static function createService ($id_donvi, $name, $number_start, $number_last, $max_number, $bg_color, $id_local, $note = null)
    {
        $service = new ServiceModel([
            'id_donvi' => $id_donvi,
            'id_localservice' => $id_local,
            'dichvu_ten' => $name,
            'dichvu_ghichu' => $note,
            'dichvu_sobatdau' => $number_start,
            'dichvu_socuoi' => $number_last,
            'dichvu_sotoida' => $max_number,
            'bg_color' => $bg_color,
            'dichvu_isSync' => 0
        ]);
        $service->save();
        return $service;
    }

    /**
     * Cập nhật dịch vụ
    **/
    public static function updateService ($service, $name, $number_start, $number_last, $max_number, $bg_color, $active, $id_local, $note = null)
    {
        $service->dichvu_ten = $name;
        $service->dichvu_ghichu = $note;
        $service->dichvu_sobatdau = $number_start;
        $service->dichvu_socuoi = $number_last;
        $service->dichvu_sotoida = $max_number;
        $service->bg_color = $bg_color;
        $service->dichvu_isActive = $active;
        $service->dichvu_isSync = 0;
        $service->id_localservice = $id_local;
        $service->save();
        return $service;
    }
}
