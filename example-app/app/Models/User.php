<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory;
    protected $table = 'user';
    protected $fillable = [
        'user_hoten',
        'username',
        'password',
        'user_phanquyen',
        'user_email',
        'user_ghichu',
        'user_giotao',
        'user_xacnhan',
        'token'
    ];

    public static function createUser ($name, $phone, $pass, $type = 1, $email = null, $note = null)
    {
        $user = new User([
            'user_hoten' => $name,
            'username' => $phone,
            'password' => bcrypt($pass),
            'user_phanquyen' => $type,
            'user_email' => $email,
            'user_ghichu' => $note,
            'user_giotao' => Carbon::now('Asia/Ho_Chi_Minh'),
            'token' => Str::random(50)
        ]);
        $user->save();
        return $user;
    }
}
