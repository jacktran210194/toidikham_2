<?php

namespace App\Console\Commands;

use App\Models\ServiceModel;
use Illuminate\Console\Command;

class UpdateService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateService';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ngay moi reset so dang goi la 0 cua dich vu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listService = ServiceModel::all();
        foreach ($listService as $key => $val){
            $listService[$key]->dichvu_sodanggoi = 0;
            $listService[$key]->dichvu_socuoi = 0;
            $listService[$key]->save();
        }
        return true;
    }
}
