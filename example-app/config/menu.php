<?php
return [
    'agency' => [
        [
            'name' => 'dashboard',
            'icon' => 'fa-tachometer-alt',
            'route' => 'agency.index',
            'title' => 'Tổng quan',
        ],
        [
            'name' => 'service',
            'icon' => 'fa-brands fa-servicestack',
            'route' => null,
            'title' => 'Dịch vụ',
            'submenu' => [
                [
                    'title' => 'Danh sách dịch vụ',
                    'route' => 'agency.service.index',
                    'name' => 'index'
                ],
                [
                    'title' => 'Danh sách khách hàng',
                    'route' => 'agency.service.customer',
                    'name' => 'customer'
                ]
            ]
        ],
        [
            'name' => 'printer',
            'icon' => 'fa-sharp fa-light fa-print',
            'title' => 'Máy in',
            'submenu' => [
                [
                    'title' => 'Danh sách máy in',
                    'route' => 'agency.printer.index',
                    'name' => 'index'
                ],
                [
                    'title' => 'Thêm mới máy in',
                    'route' => 'agency.printer.create',
                    'name' => 'create'
                ]
            ]
        ],
        [
            'name' => 'call_number',
            'icon' => 'fa-book-open',
            'route' => 'agency.call',
            'title' => 'Gọi số thứ tự'
        ],
        [
            'name' => 'account',
            'title' => 'Tài khoản',
            'icon' => 'fa-user',
            'route' => null,
            'submenu' => [
                [
                    'title' => 'Thông tin',
                    'route' => 'agency.account.index',
                    'name' => 'index'
                ],
                [
                    'title' => 'Đổi mật khẩu',
                    'route' => 'agency.account.password',
                    'name' => 'password'
                ]
            ]
        ]
    ],
    'admin' => [
        [
            'name' => 'sector',
            'title' => 'Lĩnh vực',
            'icon' => 'fa-book-open',
            'route' => 'admin.sector.index',
        ],
        [
            'name' => 'agency',
            'title' => 'Đơn vị',
            'icon' => 'fa-book-open',
            'route' => 'admin.index',
        ],
        [
            'name' => 'service',
            'title' => 'Dịch vụ',
            'icon' => 'fa-brands fa-servicestack',
            'route' => 'admin.service.index'
        ],
        [
            'name' => 'printer',
            'title' => 'Máy in',
            'icon' => 'fa-sharp fa-light fa-print',
            'route' => 'admin.printer.index'
        ],
        [
            'name' => 'user',
            'title' => 'Người dùng',
            'icon' => 'fa-book-open',
            'route' => 'admin.user.index',
        ]
    ]
]
?>
