<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;

Route::get('login', function () {
    return view('admin.login');
})->name('login');
Route::post('doLogin', [AdminController::class, 'doLogin'])->name('doLogin');
Route::middleware('check-auth-admin')->group(function () {
    Route::get('', [AdminController::class, 'index'])->name('index');
    Route::get('logout', [AdminController::class, 'logout'])->name('logout');
    Route::get('agency-delete/{id}', [AdminController::class, 'deleteAgency'])->name('agency.delete');
    Route::post('show-agency', [AdminController::class, 'showAgency']);
    Route::post('update-agency/{id}', [AdminController::class, 'updateAgency'])->name('agency.update');
    Route::post('store-agency', [AdminController::class, 'storeAgency'])->name('agency.store');

    Route::prefix('user')->name('user.')->group(function () {
        Route::get('', [AdminController::class, 'user'])->name('index');
        Route::get('delete/{id}', [AdminController::class, 'deleteUser'])->name('delete');
        Route::post('show', [AdminController::class, 'showUser'])->name('show');
        Route::post('update/{id}', [AdminController::class, 'updateUser'])->name('update');
        Route::post('store', [AdminController::class, 'storeUser'])->name('store');
    });
    Route::prefix('sector')->name('sector.')->group(function () {
        Route::get('', [AdminController::class, 'listSector'])->name('index');
        Route::get('delete/{id}', [AdminController::class, 'deleteSector'])->name('delete');
        Route::post('show', [AdminController::class, 'showSector'])->name('show');
        Route::post('update/{id}', [AdminController::class, 'updateSector'])->name('update');
        Route::post('store', [AdminController::class, 'storeSector'])->name('store');
    });

    Route::prefix('service')->name('service.')->group(function (){
        Route::get('', [AdminController::class, 'service'])->name('index');
        Route::get('delete/{id}', [AdminController::class, 'deleteService'])->name('delete');
        Route::post('show', [AdminController::class, 'showService'])->name('show');
        Route::post('update/{id}', [AdminController::class, 'updateService'])->name('update');
        Route::post('create', [AdminController::class, 'createService'])->name('create');
    });

    Route::prefix('printer')->name('printer.')->group(function (){
        Route::get('', [AdminController::class, 'printer'])->name('index');
        Route::get('delete/{id}', [AdminController::class, 'deletePrint'])->name('delete');
        Route::post('show', [AdminController::class, 'showPrint'])->name('show');
        Route::post('update/{id}', [AdminController::class, 'updatePrinter'])->name('update');
        Route::post('create', [AdminController::class, 'createPrinter'])->name('create');
    });
});
