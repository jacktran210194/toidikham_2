<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('lay-so-thu-tu', [APIController::class, 'getSTT']);
Route::post('goi-so-thu-tu', [APIController::class, 'callNumber']);
Route::post('get-token', [APIController::class, 'getToken']);
Route::post('dongbodonvi', [APIController::class, 'dongBoDonVi']);
Route::post('dongbodichvu', [APIController::class, 'dongBoDichVu']);
Route::post('dongbomayin', [APIController::class, 'dongBoMayIn']);
Route::post('dongbokhachhang', [APIController::class, 'dongBoKhachHang']);
Route::post('khach-hang-lay-so-thu-tu', [APIController::class, 'khachHangLaySoThuTu']);
