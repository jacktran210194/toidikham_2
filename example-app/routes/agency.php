<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AgencyController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('dang-nhap', function () {
return view('agency.login');
})->name('login');
Route::post('doLogin', [AgencyController::class, 'doLogin'])->name('doLogin');
Route::middleware('check-auth-agency')->group(function () {
    Route::get('', [AgencyController::class, 'index'])->name('index');
        Route::prefix('goi-so-thu-tu')->group(function () {
        Route::get('', [AgencyController::class, 'callNumber'])->name('call');
        Route::get('{id}', [AgencyController::class, 'callNumberService'])->name('call.service');
    });
    Route::prefix('dich-vu')->name('service.')->group(function () {
        Route::get('', [AgencyController::class,'service'])->name('index');
        Route::post('tạo mới', [AgencyController::class, 'createService'])->name('create');
        Route::post('cap-nhat/{id}', [AgencyController::class, 'updateService'])->name('update');
        Route::get('delete/{id}', [AgencyController::class, 'deleteService'])->name('delete');
        Route::get('xuat-excel/{id}', [AgencyController::class, 'excel'])->name('export-excel');
        Route::post('chinh-sua', [AgencyController::class, 'modalService']);
        Route::get('khach-hang', [UserController::class, 'customer'])->name('customer');
        Route::get('{id}', [AgencyController::class, 'detailsService'])->name('details');
    });
    Route::get('logout', [AgencyController::class, 'logOut'])->name('logout');
    Route::post('get-data-area-chart', [AgencyController::class, 'getDataAreaChart']);
    Route::post('get-data-bar-chart', [AgencyController::class, 'getDataBarChart']);
    Route::get('xuat-excel', [AgencyController::class, 'excelIndex'])->name('export-excel');
    Route::prefix('tai-khoan')->name('account.')->group(function () {
        Route::get('', [UserController::class, 'account'])->name('index');
        Route::post('cap-nhat', [UserController::class, 'update'])->name('update');
        Route::get('doi-mat-khau', [UserController::class, 'password'])->name('password');
        Route::post('cap-nhat-mat-khau', [UserController::class, 'updatePassword'])->name('update_password');
    });
    Route::prefix('may-in')->name('printer.')->group(function (){
        Route::get('', [UserController::class,'printer'])->name('index');
        Route::get('them-moi', [UserController::class, 'createPrinter'])->name('create');
        Route::post('store', [UserController::class, 'storePrinter'])->name('store');
        Route::get('delete/{id}', [UserController::class, 'deletePrinter'])->name('delete');
        Route::post('thong-tin', [UserController::class, 'infoPrinter'])->name('info');
        Route::post('update/{id}', [UserController::class, 'updatePrinter'])->name('update');
    });
});
