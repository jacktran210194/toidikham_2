<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('dang-nhap', function (){
    return view('login');
})->name('login');

Route::get('dang-ky', function (){
    return view('sign');
})->name('sign');

Route::post('doLogin', [DashboardController::class, 'doLogin'])->name('doLogin');
Route::post('register', [DashboardController::class, 'register'])->name('register');

Route::get('', [DashboardController::class, 'index'])->name('home');
// Lấy danh sách đơn vị //
Route::post('get-agency', [DashboardController::class, 'getAgency']);
// Lấy danh sách dịch vụ //
Route::post('get-service', [DashboardController::class, 'getService']);
// Lấy số thứ tự //
Route::post('order', [DashboardController::class, 'order'])->name('order');
// Tra cứ số thứ tự của khách hàng //
Route::get('tra-cuu-stt', [DashboardController::class, 'filter'])->name('filter');
