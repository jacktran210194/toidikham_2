<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrinterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mayin', function (Blueprint $table) {
            $table->id();
            $table->integer('id_donvi')->comment('Mã đơn vị');
            $table->string('mayin_tenmay')->nullable()->comment('Tên máy in');
            $table->string('mayin_uid')->nullable()->comment('Mã UID máy in');
            $table->string('mayin_diachi')->nullable()->comment('Địa chỉ đặt máy');
            $table->string('mayin_line1')->nullable()->comment('Ghi chú dòng 1');
            $table->string('mayin_line2')->nullable()->comment('Ghi chú dòng 2');
            $table->tinyInteger('mayin_isQR')->nullable()->comment('In QR không?');
            $table->integer('mayin_solien')->comment('Số liên');
            $table->tinyInteger('mayin_isActive')->default(1)->comment('Kích hoạt không?');
            $table->time('mayin_startMorning')->nullable()->comment('Bắt đầu sáng');
            $table->time('mayin_endMorrning')->nullable()->comment('Hết giờ sáng');
            $table->time('mayin_startAfternoon')->nullable()->comment('Bắt đầu chiều');
            $table->time('mayin_endAfternoon')->nullable()->comment('Hết giờ chiều');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('printer');
    }
}
