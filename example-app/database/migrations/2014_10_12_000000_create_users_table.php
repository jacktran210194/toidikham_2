<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->id();
            $table->string('user_hoten')->comment('Tên người dùng');
            $table->string('username')->unique()->comment('Tên đăng nhập');
            $table->integer('user_phanquyen')->comment('Phân quyền : 1 user, 2 Don vi')->default(1);
            $table->string('user_email')->nullable()->comment('Email');
            $table->string('password');
            $table->string('user_ghichu')->nullable()->comment('Ghi chú');
            $table->timestamp('user_xacnhan')->nullable()->comment('Giờ xác nhận email');
            $table->timestamp('user_giotao')->nullable()->comment('Giờ tạo');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
