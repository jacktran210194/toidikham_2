<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Linhvuc', function (Blueprint $table) {
            $table->id();
            $table->string('linhvuc_ten')->comment('Tên lĩnh vực');
            $table->string('linhvuc_ghichu')->nullable()->comment('Ghi chú');
            $table->timestamp('linhvuc_giotao')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sector');
    }
}
