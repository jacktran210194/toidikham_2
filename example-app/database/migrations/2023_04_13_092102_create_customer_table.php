<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Khachhang', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('id_donvi')->comment('Mã đơn vị');
            $table->string('ten_donvi')->comment('Tên đơn vị');
            $table->integer('id_dichvu')->comment('Mã dịch vụ');
            $table->string('ten_dichvu')->comment('Tên dịch vụ');
            $table->integer('id_mayin')->comment('Mã Printer');
            $table->string('khach_sothutu')->comment('Số thứ tự');
            $table->string('ma_sothutu');
            $table->timestamp('khach_giolayso')->comment('Giờ lấy số');
            $table->timestamp('khach_giogoiso')->nullable()->comment('Giờ gọi số');
            $table->timestamp('khach_gioketthuc')->nullable()->comment('Giờ kết thúc');
            $table->integer('khach_thoigiancho')->nullable()->comment('Thời gian chờ');
            $table->integer('khach_thoiganphucvu')->nullable()->comment('Thời gian phuc vu');
            $table->string('khach_tenkhach')->nullable()->comment('Tên khách hàng');
            $table->string('khach_dienthoai')->comment('Số điện thoại');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
