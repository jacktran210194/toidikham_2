<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Dichvu', function (Blueprint $table) {
            $table->id();
            $table->integer('id_donvi')->comment('Mã đơn vị');
            $table->integer('id_localservice')->nullable();
            $table->string('dichvu_ten')->comment('Tên dịch vụ');
            $table->integer('dichvu_sobatdau')->comment('Số bắt đầu');
            $table->integer('dichvu_socuoi')->default(0)->comment('Số cuối cùng');
            $table->integer('dichvu_sodanggoi')->default(0)->comment('Số đang gọi');
            $table->integer('dichvu_sosttdanglay')->default(0)->comment('Số đang gọi');
            $table->integer('dichvu_sotoida')->nullable()->comment('Số tối đa 1 ngày');
            $table->integer('dichvu_isActive')->default(1)->comment('Kích hoạt?');
            $table->string('dichvu_ghichu')->nullable()->comment('Ghi chú');
            $table->timestamp('thoigian_capnhat')->nullable()->comment('Thời gian cập nhật số cuối');
            $table->string('bg_color')->nullable()->comment('Mã màu');
            $table->tinyInteger('dichvu_isSync')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service');
    }
}
