<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donvi', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('id_linhvuc')->comment('Mã lĩnh vực');
            $table->string('donvi_ten')->comment('Tên đơn vị');
            $table->string('donvi_diachi')->comment('Địa chỉ đơn vị')->nullable();
            $table->string('donvi_ghichu')->comment('Ghi chú')->nullable();
            $table->integer('donvi_isActive')->default(0)->comment('Kích hoạt: 0 chưa kích hoạt, 1 đã kích hoạt');
            $table->boolean('donvi_isSync')->default(false);
            $table->timestamp('donvi_giotao')->comment('Giờ tạo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency');
    }
}
