$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(".btn-delete").click(function () {
    let service_id = $(this).val();
    let url = window.location.origin + '/don-vi/dich-vu/delete/'+service_id;
    $.confirm({
        title: 'Xác nhận!',
        content: 'Bạn chắc xóa dịch vụ này không?',
        buttons: {
            confirm: {
                text: 'Xác nhận',
                btnClass: 'btn-blue',
                action: function(){
                    location.replace(url);
                }
            },
            cancel: {
                text: 'Hủy',
                btnClass: 'btn-danger',
                action: function(){}
            }
        }
    });
});
$(".btn-edit-service").click(function () {
    let service_id = $(this).val();
    $.ajax({
        url: window.location.origin + '/don-vi/dich-vu/chinh-sua',
        data: {'service_id' : service_id},
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.status){
                $("#modalEditService .modal-body").html(data.html);
                $("#modalEditService").modal("show");
            }else{
                alert(data.msg);
            }
        }
    })
});
$(document).on("click", ".btn-close-modal", function () {
    $("#modalEditService").modal("hide");
});
$(".btn-export-excel").click(function (ev) {
    ev.preventDefault();
    var href = $(this).attr('href');
    var url = href + '?date_form='+$('input[name="date_form"]').val()+'&date_to='+$('input[name="date_to"]').val();
    location.replace(url)
});
