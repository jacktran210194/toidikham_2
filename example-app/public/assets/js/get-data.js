$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('select[name="sector"]').change(function () {
    let value = $(this).val();
    get_option($('select[name="agency"]'), '/get-agency', value);
});
$(document).on( 'change', 'select[name="agency"]', function () {
    let value = $(this).val();
    get_option($('select[name="service"]'), '/get-service', value);
});
function get_option(element, url, value) {
    $.ajax({
        url: window.location.origin + url,
        data: {'value' : value},
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if(data.status){
                element.html(data.html);
            }
        }
    });
}
let a,b;
$('.btn-success[type="submit"]').click(function () {
    var phone = $('input[name="phone"]').val();
    var agency = $('select[name="agency"]').val();
    var service = $('select[name="service"]').val();
    if ( phone === ''){
        alert('Vui lòng điền số điện thoại');
        return false;
    }
    if ( agency === ''){
        alert('Vui lòng chọn đơn vị');
        return false;
    }
    if ( service === ''){
        alert('Vui lòng chọn dịch vụ');
        return false;
    }
    a = Math.floor(Math.random() * 10);
    b = Math.floor(Math.random() * 10);
    $("#number_1").text(a);
    $("#number_2").text(b);
    $("#exampleModal").modal("show");
    setTimeout(function () {
        $('#result').focus();
    },500);
});
$(".btn-get-stt").click(function () {
    checkSpam();
});
function get_stt() {
    let data = {};
    data['name'] = $('input[name="name"]').val();
    data['phone'] = $('input[name="phone"]').val();
    data['sector'] = $('select[name="sector"]').val();
    data['agency'] = $('select[name="agency"]').val();
    data['service'] = $('select[name="service"]').val();
    $.ajax({
        url: window.location.origin + '/order',
        type: 'post',
        data: data,
        dataType: 'json',
        success: function (data) {
            $("#exampleModal").modal("hide");
            if (data.status){
                let html = document.getElementById("html-content-holder");
                $("#html-content-holder").html(data.html);
                $("#exampleModalCenter").modal('show');
                setTimeout(function () {
                    html2canvas(html).then(function (canvas) {
                        let anchorTag = document.createElement("a");
                        document.body.appendChild(anchorTag);
                        anchorTag.download = data.file_name+".jpg";
                        anchorTag.href = canvas.toDataURL();
                        anchorTag.click();
                    });
                },200);
            }else{
                alert(data.msg);
            }
        }
    });
}
function checkSpam() {
    var result = $('input[name="result"]').val();
    if (parseInt(result) != (a+b)){
        alert('Kết quả không chính xác');
    }else{
        $('input[name="result"]').val('');
        get_stt();
    }
}
